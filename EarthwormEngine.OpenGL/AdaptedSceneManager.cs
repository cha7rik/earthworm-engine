﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EarthwormEngine;

namespace OpenGL
{
    public class AdaptedSceneManager : EarthwormEngine.SceneManager
    {
        public new AdaptedContentManager Content;

        public AdaptedSceneManager(SceneManagerConfiguration config) : base(config)
        {
            //Content = new AdaptedContentManager(Services, "Content", "");
            Content = new AdaptedContentManager(Services, "Content", "1111");
        }

        public override IContentManager GetContentManager()
        {
            return Content;
        }
    }
}
