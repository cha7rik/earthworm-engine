﻿/* 
 * Example Content Manager with encryped zip content loading implemented
 */
using System;
using System.Diagnostics;
using System.IO;
using EarthwormEngine;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Xna.Framework;

namespace OpenGL
{
    public class AdaptedContentManager : EarthwormEngine.ContentManager, IContentManager
    {
        private bool usingZip;
        private ZipFile zipFile;

        public AdaptedContentManager(GameServiceContainer gsc, string rootDirectory, string ZipPassword) : base(gsc,rootDirectory)
        {
            usingZip = false;

            if (!string.IsNullOrEmpty(ZipPassword))
            {
                //using zip
                if (File.Exists(Path.Combine(RootDirectory + ".dat")))
                {
                    zipFile = new ZipFile(Path.Combine(this.RootDirectory + ".dat"));
                    zipFile.Password = ZipPassword;
                    usingZip = true;
                }
                else
                {
                    Debug.WriteLine("[ERROR] AdaptedContentManager : Content.dat cannot be found.");
                    usingZip = false;
                }
            }
            else
            {
                if (Directory.Exists(rootDirectory))
                {
                    RootDirectory = rootDirectory;
                }
                else
                    Debug.WriteLine("[ERROR] AdaptedContentManager : Content folder cannot be found.");
            }
        }

        protected override Stream OpenStream(string assetName)
        {
            if (usingZip)
            {
                assetName = assetName.Replace('\\', '/');

                //read file from zip
                int entry = zipFile.FindEntry(Path.Combine(RootDirectory + "/", assetName), true);
                if (entry != -1)
                    return zipFile.GetInputStream(entry);
                else
                {
                    entry = zipFile.FindEntry(Path.Combine(RootDirectory + "/", assetName + ".xnb"), true);
                    if (entry != -1)
                        return zipFile.GetInputStream(entry);
                    else //particle file
                    {
                        entry = zipFile.FindEntry(Path.Combine(RootDirectory + "/", assetName + ".part"), true);
                        if (entry != -1)
                            return zipFile.GetInputStream(entry);
                    }
                }
            }
            else
            { 
                if (File.Exists(Path.Combine(RootDirectory, assetName)))
                {
                    return new FileStream(Path.Combine(RootDirectory, assetName), FileMode.Open, FileAccess.Read, FileShare.None);
                }
            }

            return base.OpenStream(assetName);
        }

        /// <summary>
        /// Load any non-build content (custom content) from Monogame Content Pipeline
        /// </summary>
        /// <param name="path">File Path</param>
        /// <returns>StreamReader</returns>
        public override StreamReader StreamReader(string path)
        {
            if (usingZip)
            {
                path = path.Replace('\\', '/');

                int entry = zipFile.FindEntry(path, true);
                if (entry != -1)
                    return new StreamReader(zipFile.GetInputStream(entry));
                else
                {
                    Debug.WriteLine("[ERROR] AdaptedContentManager.StreamReader : File not found.");
                    return null;
                }
            }
            else
            {
                return new StreamReader(TitleContainer.OpenStream(path));
            }
        }
    }
}
