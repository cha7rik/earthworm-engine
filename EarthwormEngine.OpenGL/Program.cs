﻿using System;
using EarthwormEngine;
using EarthwormEngine.Scenes;

namespace OpenGL
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SceneManagerConfiguration config = new SceneManagerConfiguration();
            config.BackgroundColor = Microsoft.Xna.Framework.Color.CornflowerBlue;
            config.InitialScene = new SceneTest();
            config.WindowResolution = new Microsoft.Xna.Framework.Vector2(960, 640);
            config.GameResolution = new Microsoft.Xna.Framework.Vector2(960, 640);
            config.IsFullScreen = false;
            config.ActivePlatform = PlatformDevice.DESKTOPGL;
            AdaptedSceneManager sm = new AdaptedSceneManager(config);
            using (var game = sm)
                game.Run();
        }
    }
}
