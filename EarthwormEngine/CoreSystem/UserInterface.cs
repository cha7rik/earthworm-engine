﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using EarthwormEngine.InputSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// User interface for game
    /// </summary>
    public abstract class UserInterface
    {
        protected CoreScene currentScene;
        public Vector2 Position { get { return currentPosition; } set { currentPosition = value; TargetPosition = value; StartPosition = value; } }
        protected Vector2 currentPosition;
        public float LayerDepth = 0.9f;
        public bool Visible = true;
        public Texture2D Texture;
        //the speed solution use for calculate interpolation movement
        protected Func<float, float, float> interpolateFunction;
        private float interpolateTime = 0.0f;
        private float interpolateTimeScale = 1.0f;
        private Vector2 StartPosition;
        private Vector2 TargetPosition;

        public UserInterface()
        {
            interpolateFunction = DefaultInterpolateFunction;
        }

        public virtual void Init()
        {
        }

        public virtual void Load(IContentManager content)
        {

        }

        public abstract void Draw(SpriteBatch spriteBatch);

        public virtual void Update()
        {
            if (currentPosition != TargetPosition && interpolateTime < 1.0f)
            {
                interpolateTime += 1 / (interpolateTimeScale / (float)Calculator.GetElapsedGameTime());
                currentPosition = new Vector2(
                    Calculator.LinearInterpolation(StartPosition.X, TargetPosition.X, interpolateFunction(interpolateTime,0.0f)),
                    Calculator.LinearInterpolation(StartPosition.Y, TargetPosition.Y, interpolateFunction(interpolateTime,0.0f)));
            }
            else
            {
                interpolateTime = 0.0f;
                Position = TargetPosition;
            }
        }

        public virtual void Unload()
        {

        }

        /// <summary>
        /// Move UI to position with interpolation
        /// </summary>
        /// <param name="position">New position</param>
        /// <param name="time">Time to move (Second)</param>
        /// <returns>Is reach to new position</returns>
        public bool MovePositionTo(Vector2 position, float time)
        {
            TargetPosition = position;
            interpolateTimeScale = time;
            StartPosition = Position;
            interpolateFunction = DefaultInterpolateFunction;

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }

        /// <summary>
        /// Move UI to position with interpolation
        /// </summary>
        /// <param name="position">New position</param>
        /// <param name="time">Time to move (Second)</param>
        /// <param name="solution">Custom solution for liner movement speed</param>
        /// <returns>Is reach to new position</returns>
        public bool MovePositionTo(Vector2 position, float time, Func<float, float, float> solution)
        {
            TargetPosition = position;
            interpolateTimeScale = time;
            StartPosition = Position;
            interpolateFunction = solution;

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }

        /// <summary>
        /// Move UI to position with interpolation
        /// </summary>
        /// <param name="position">New position</param>
        /// <param name="time">Time to move (Second)</param>
        /// <param name="isForceMove">Force to move to new position even it not finish moving (not recomend)</param>
        /// <returns>Is reach to new position</returns>
        public bool MovePositionTo(Vector2 position, float time, bool isForceMove)
        {
            if (!isForceMove)
            {
                if (interpolateTime == 0)
                {
                    TargetPosition = position;
                    interpolateTimeScale = time;
                    StartPosition = Position;
                    interpolateFunction = DefaultInterpolateFunction;
                }
            }
            else
            {
                TargetPosition = position;
                interpolateTimeScale = time;
                StartPosition = Position;
                interpolateTime = 0;
                interpolateFunction = DefaultInterpolateFunction;
            }

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }

        /// <summary>
        /// Move UI to position with interpolation
        /// </summary>
        /// <param name="position">New position</param>
        /// <param name="time">Time to move (Second)</param>
        /// <param name="solution">Custom solution for liner movement speed</param>
        /// <param name="isForceMove">Force to move to new position even it not finish moving (not recomend)</param>
        /// <returns>Is reach to new position</returns>
        public bool MovePositionTo(Vector2 position, float time, Func<float, float, float> solution, bool isForceMove)
        {
            if (!isForceMove)
            {
                if (interpolateTime == 0)
                {
                    TargetPosition = position;
                    interpolateTimeScale = time;
                    StartPosition = Position;
                    interpolateFunction = solution;
                }
            }
            else
            {
                TargetPosition = position;
                interpolateTimeScale = time;
                StartPosition = Position;
                interpolateTime = 0;
                interpolateFunction = solution;
            }

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }

        //default solution for linear interpolation movement
        protected float DefaultInterpolateFunction(float x, float n)
        {
            //while x is between 0 to 1
            
            //ease in ease out
            return (float)Math.Pow(x, 2) * (3.0f - 2.0f * x);

            //ease in
            //return (float)Math.Pow(x, 2);

            //ease out
            //return x*(2-x);
        }
    }

    public class UITextBox : UserInterface
    {
        public string Text {get;set;}
        public string Font { get; set; }
        public bool IsCenter {get;set;}
        public Color TextColor { get; set; }
        public float Opacity { get; set; }
        public Vector2 Scale { get; set; }
        public float Angle { get; set; }
        public Vector2 Pivot { get; set; }
        SpriteFont spriteFont;
        public bool Enable { get; set; }

        /// <summary>
        /// Create textbox as UI
        /// </summary>
        /// <param name="pos">Text Position</param>
        /// <param name="text">Text</param>
        /// <param name="font">Text Font</param>
        /// <param name="color">Text Color</param>
        /// <param name="scale">Text Scale</param>
        /// <param name="angle">Text Rotation</param>
        /// <param name="isCenter"></param>
        /// <param name="CurrentScene">Scene which this textbox shown</param>
        public UITextBox(Vector2 pos, string text, string font, Color color, Vector2 scale, float angle, bool isCenter, CoreScene CurrentScene)
        {
            currentScene = CurrentScene;
            Text = text;
            base.Position = pos;
            Font = font;
            Opacity = 1.0f;
            Scale = scale;
            TextColor = color;
            Angle = angle;
            Pivot = Vector2.Zero;
            IsCenter = isCenter;
            base.LayerDepth = 0.9f;
            Enable = true;
            Init();
        }
        /// <summary>
        /// Create textbox as UI (but faster)
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="CurrentScene"></param>
        public UITextBox(Vector2 pos, string text, string font, CoreScene CurrentScene)
        {
            currentScene = CurrentScene;
            Text = text;
            base.Position = pos;
            Font = font;
            Opacity = 1.0f;
            Scale = Vector2.One;
            TextColor = Color.White;
            Angle = 0.0f;
            Pivot = Vector2.Zero;
            IsCenter = false;
            base.LayerDepth = 0.9f;
            Enable = true;
            Init();
        }

        public override void Init()
        {
            
            base.Init();
        }

        public override void Load(IContentManager content)
        {
            spriteFont = content.Load<SpriteFont>(Font);
            base.Load(content);
        }
        public override void Update()
        {
            base.Update();
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (IsCenter && !string.IsNullOrEmpty(Text))
                Pivot = new Vector2(spriteFont.MeasureString(Text).X / 2, 0);
            if (base.Visible)
                spriteBatch.DrawString(spriteFont, Text, currentPosition, TextColor*Opacity, Calculator.DegreeToRadians(Angle), Pivot, Scale, SpriteEffects.None, base.LayerDepth);
        }
        public override void Unload()
        {

            base.Unload();
        }
    }

    public class UISprite : UserInterface
    {
        /// <summary>
        /// Directory of texture with no extension
        /// </summary>
        public string TextureName;
        public Vector2 Pivot;
        public float Angle;
        /// <summary>
        /// Scale by X and Y (1.0f is Default)
        /// </summary>
        public Vector2 Scale;
        public Color Color;
        public float Opacity;
        /// <summary>
        /// Sprite cut for animate
        /// </summary>
        public Vector2 SpriteCut;
        public Animation SpriteAnimation;

        /// <summary>
        /// Create sprite as UI
        /// </summary>
        /// <param name="texture">Sprite texture directory without extension</param>
        /// <param name="pos">Sprite position</param>
        /// <param name="scale">Scale by X and Y (1.0f is Default)</param>
        /// <param name="color">Sprite color tint</param>
        /// <param name="angle">Sprite rotation</param>
        /// <param name="CurrentScene">Scene which this sprite shown</param>
        public UISprite(string texture, Vector2 pos, Vector2 scale, Color color, float angle, CoreScene CurrentScene)
        {
            currentScene = CurrentScene;
            TextureName = texture;
            Pivot = Vector2.Zero;
            base.Position = pos;
            Scale = scale;
            Color = color;
            Angle = angle;
            Opacity = 1.0f;
            SpriteCut = Vector2.Zero;
            Init();
        }

        /// <summary>
        /// Create sprite as UI
        /// </summary>
        /// <param name="texture">Sprite texture directory without extension</param>
        /// <param name="pos">Sprite position</param>
        /// <param name="scale">Scale by X and Y (1.0f is Default)</param>
        /// <param name="color">Sprite color tint</param>
        /// <param name="angle">Sprite rotation</param>
        /// <param name="spriteCut">Sprite cut size for animate</param>
        /// <param name="CurrentScene">Scene which this sprite shown</param>
        public UISprite(string texture, Vector2 pos, Vector2 scale, Color color, float angle, Vector2 spriteCut, CoreScene CurrentScene)
        {
            currentScene = CurrentScene;
            TextureName = texture;
            Pivot = Vector2.Zero;
            base.Position = pos;
            Scale = scale;
            Color = color;
            Angle = angle;
            Opacity = 1.0f;
            SpriteCut = spriteCut;
            Init();
        }

        public override void Load(IContentManager content)
        {
            base.Load(content);

            if (!string.IsNullOrEmpty(TextureName))
            {
                Texture = content.Load<Texture2D>(TextureName);
                SpriteAnimation = new Animation(new Rectangle(0, 0, Texture.Width, Texture.Height), SpriteCut);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (base.Visible && Texture != null)
                spriteBatch.Draw(Texture, currentPosition, SpriteAnimation.GetSpriteAnimation(), Color * Opacity, Calculator.DegreeToRadians(Angle), Pivot, Scale, SpriteEffects.None, base.LayerDepth);
        }
    }

    public class UIButton : UserInterface
    {

        /// <summary>
        /// Directory of texture with no extension
        /// </summary>
        public string TextureName;
        public Vector2 Pivot;
        public float Angle;
        /// <summary>
        /// Scale by X and Y (1.0f is Default)
        /// </summary>
        public Vector2 Scale;
        public Color Color;
        public float Opacity;

        public Animation SpriteAnimation;

        public UIButtonState ButtonState { get; private set; }

        /// <summary>
        /// Create sprite button
        /// </summary>
        /// <param name="texture">Must split in to 3 parts in vertical, top is normal, middle is over, and bottom is on click</param>
        /// <param name="pos"></param>
        /// <param name="scale"></param>
        /// <param name="color"></param>
        /// <param name="angle"></param>
        /// <param name="CurrentScene"></param>
        public UIButton(string texture, Vector2 pos, Vector2 scale, Color color, float angle, CoreScene CurrentScene)
        {
            currentScene = CurrentScene;
            TextureName = texture;
            Pivot = Vector2.Zero;
            base.Position = pos;
            Scale = scale;
            Color = color;
            Angle = angle;
            Opacity = 1.0f;
            ButtonState = UIButtonState.Normal;
            Init();
        }

        public override void Load(IContentManager content)
        {
            base.Load(content);

            if (!string.IsNullOrEmpty(TextureName))
            {
                Texture = content.Load<Texture2D>(TextureName);

                SpriteAnimation = new Animation(new Rectangle(0, 0, Texture.Width, Texture.Height), new Vector2(Texture.Width, Texture.Height / 3));
            }
        }

        /// <summary>
        /// Update will check state of button
        /// </summary>
        public override void Update()
        {
            base.Update();

            //check button state
            //over
            if (MouseController.GetCursorPosition().X > Position.X
                && MouseController.GetCursorPosition().X < Position.X + Texture.Width
                && MouseController.GetCursorPosition().Y > Position.Y
                && MouseController.GetCursorPosition().Y < Position.Y + Texture.Height/3)
            {
                //over
                SpriteAnimation.SetFrame(1);
                ButtonState = UIButtonState.Over;
                if (MouseController.ButtonDown(MouseButton.MouseButtonLeft))
                {
                    //down
                    SpriteAnimation.SetFrame(2);
                    ButtonState = UIButtonState.Down;
                }
            }
            else
            {
                //normal
                SpriteAnimation.SetFrame(0);
                ButtonState = UIButtonState.Normal;
            }

            //check if button clicked
            if (MouseController.ButtonRelease(MouseButton.MouseButtonLeft))
            {
                //check position
                if (MouseController.GetCursorPosition().X < Position.X) return;
                if (MouseController.GetCursorPosition().X > Position.X + Texture.Width) return;
                if (MouseController.GetCursorPosition().Y < Position.Y) return;
                if (MouseController.GetCursorPosition().Y > Position.Y + Texture.Height/3) return;

                ButtonState = UIButtonState.Release;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (base.Visible && Texture != null)
                spriteBatch.Draw(Texture, currentPosition, SpriteAnimation.GetSpriteAnimation(), Color * Opacity, Calculator.DegreeToRadians(Angle), Pivot, Scale, SpriteEffects.None, base.LayerDepth);
        }
    }

    public enum UIButtonState
    {
        Normal,
        Over,
        Down,
        Release
    }
}