﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using EarthwormEngine.CollisionSystem;

namespace EarthwormEngine.ParticleSystem
{
    /// <summary>
    /// Particle class, manage a particle
    /// </summary>
    public class Particle
    {
        public Texture2D Texture;
        public Vector3 Position;
        public Vector3 Gravity;
        public Vector3 Diraction;
        public float Angle;
        public float AngularVelocity;
        public Color Color;
        public Vector4 StartColor;
        public Vector4 MaxColor;
        public float Size;
        public float MaxSize;
        public float TTL;
        public float MaxTTL;
        public float Depth;
        public Animation SpriteAnimation;
        public int animateTime;
        public int animateTimeCount;
        public int tileZ;
        public MapOrientation mapOrientation;

        private Vector4 gapColor;
        private Vector4 currentColor;
        private float gapSize;
        private float alpha;
        private float gapAlpha;
        private Vector2 SpriteCut;

        /// <summary>
        /// Particle's Collision Box
        /// </summary>
        public CollisionBox collisionBox;

        /// <summary>
        /// Particle active flag
        /// </summary>
        public bool Active;

        /// <summary>
        /// Create uninitialize particle
        /// </summary>
        public Particle() { }

        /// <summary>
        /// Create particle
        /// </summary>
        /// <param name="texture">Particle Texture</param>
        /// <param name="position">Particle Position</param>
        /// <param name="tileZsize">map tile height, use for calculate y position by z axis </param>
        /// <param name="gravity">Particle gravity</param>
        /// <param name="velocity">Particle velocity</param>
        /// <param name="angle">Particle rotation angle</param>
        /// <param name="angularVelocity">Particle rotation speed</param>
        /// <param name="startColor">Particle color at beginning</param>
        /// <param name="maxColor">Particle color at end</param>
        /// <param name="size">Particle size at start</param>
        /// <param name="maxSize">Particle size at end</param>
        /// <param name="ttl">Particle time to live</param>
        /// <param name="depth">Particle layer depth</param>
        /// <param name="spriteCut">Particle frame size in case particle have more that one frame</param>
        public Particle(Texture2D texture, Vector3 position, int tileZsize,Vector3 gravity, Vector3 velocity,
            float angle, float angularVelocity, Vector4 startColor, Vector4 maxColor, float size, float maxSize, float ttl, float depth,
            Vector2 spriteCut)
        {
            Texture = texture;
            Position = position;
            Diraction = velocity;
            Gravity = gravity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            StartColor = startColor;
            MaxColor = maxColor;
            Color = new Color(startColor.X, startColor.Y, startColor.Z, 255);
            currentColor = startColor;
            Size = size;
            MaxTTL = ttl;
            TTL = MaxTTL;
            Depth = depth;
            MaxColor = maxColor;
            MaxSize = maxSize;
            tileZ = tileZsize;

            alpha = startColor.W;

            //calculate color gap
            gapColor = (MaxColor - startColor) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            gapAlpha = (maxColor.W - startColor.W) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            //calculate size gap
            gapSize = (MaxSize - size) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));

            //animation
            SpriteAnimation = new Animation(new Rectangle(0,0,Texture.Width, Texture.Height), spriteCut);
            animateTime = 0;
            animateTimeCount = animateTime;
            SpriteCut = spriteCut;

            mapOrientation = MapOrientation.Orthogonal;

            Active = true;
        }

        /// <summary>
        /// Create particle with animate initialize
        /// </summary>
        /// <param name="texture">Particle Texture</param>
        /// <param name="position">Particle Position</param>
        /// <param name="tileZsize">map tile height, use for calculate y position by z axis </param>
        /// <param name="gravity">Particle gravity</param>
        /// <param name="velocity">Particle velocity</param>
        /// <param name="angle">Particle rotation angle</param>
        /// <param name="angularVelocity">Particle rotation speed</param>
        /// <param name="startColor">Particle color at beginning</param>
        /// <param name="maxColor">Particle color at end</param>
        /// <param name="size">Particle size at start</param>
        /// <param name="maxSize">Particle size at end</param>
        /// <param name="ttl">Particle time to live</param>
        /// <param name="depth">Particle layer depth</param>
        /// <param name="spriteCut">Particle frame size in case particle have more that one frame</param>
        /// <param name="animationTime">Particle animation frame delay</param>
        public Particle(Texture2D texture, Vector3 position,int tileZsize, Vector3 gravity, Vector3 velocity,
            float angle, float angularVelocity, Vector4 startColor, Vector4 maxColor, float size, float maxSize, float ttl, float depth,
            Vector2 spriteCut, int animationTime)
        {
            Texture = texture;
            Position = position;
            Diraction = velocity;
            Gravity = gravity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            StartColor = startColor;
            MaxColor = maxColor;
            Color = new Color(startColor.X, startColor.Y, startColor.Z, 255);
            currentColor = startColor;
            Size = size;
            MaxTTL = ttl;
            TTL = MaxTTL;
            Depth = depth;
            MaxColor = maxColor;
            MaxSize = maxSize;
            tileZ = tileZsize;

            alpha = startColor.W;

            //calculate color gap
            gapColor = (MaxColor - startColor) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            gapAlpha = (maxColor.W - startColor.W) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            //calculate size gap
            gapSize = (MaxSize - size) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));

            //animation
            SpriteAnimation = new Animation(new Rectangle(0,0,Texture.Width, Texture.Height), spriteCut);
            animateTime = animationTime;
            animateTimeCount = animateTime;
            SpriteCut = spriteCut;

            mapOrientation = MapOrientation.Orthogonal;

            Active = true;
        }

        /// <summary>
        /// Update particle
        /// </summary>
        public void Update()
        {
            if (Active)
            {
                TTL -= Calculator.UnitsPerSecound(1.0f);
                Diraction += Calculator.UnitsPerSecound(Gravity);
                Position += Diraction;
                Angle += AngularVelocity;
                //change size
                Size += gapSize;
                //change color
                currentColor += gapColor;
                Color.R = (byte)currentColor.X;
                Color.G = (byte)currentColor.Y;
                Color.B = (byte)currentColor.Z;
                //calculate alpha
                alpha += gapAlpha;

                //animate
                if (SpriteAnimation.GetTotalFrame() > 1 && animateTime > 0)
                {
                    if (animateTimeCount > 0)
                        animateTimeCount--;
                    else
                    {
                        if (SpriteAnimation.CurrentFrame == SpriteAnimation.GetTotalFrame() - 1)
                            SpriteAnimation.SetFrame(0);
                        else
                            SpriteAnimation.SetFrame(SpriteAnimation.CurrentFrame + 1);

                        animateTimeCount = animateTime;
                    }
                }
            }
        }
        /// <summary>
        /// Draw particle
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (Active)
            {
                //calculate position according to z axis
                Vector2 drawPos = new Vector2(Position.X, Position.Y - (Position.Z * tileZ));

                spriteBatch.Draw(Texture, drawPos, SpriteAnimation.GetSpriteAnimation(), Color * (Calculator.GetPercentage(alpha, 255) / 100.0f),
                    Angle, SpriteCut / 2, Size, SpriteEffects.None, Depth);
            }
        }

        /// <summary>
        /// Draw particle
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="cameraViewPort">Use for optimize drawing</param>
        public void Draw(SpriteBatch spriteBatch, Rectangle cameraViewPort)
        {
            if (Active)
            {
                //calculate position according to z axis
                Vector2 drawPos = new Vector2(Position.X, Position.Y + Position.Z);

                //not draw if out of viewport
                if (drawPos.X + (SpriteCut.X / 2) * Size < cameraViewPort.X
                    || drawPos.Y + (SpriteCut.Y / 2) * Size < cameraViewPort.Y
                    || drawPos.X - (SpriteCut.X / 2) * Size > cameraViewPort.Width
                    || drawPos.Y - (SpriteCut.Y / 2) * Size > cameraViewPort.Height)
                    return;

                //calculate depth
                decimal depth = (decimal)Math.Round(Depth, 2);

                if (mapOrientation == MapOrientation.Isometric)
                {
                    //calculate y axis layer by tile height for isometric map
                    depth += (decimal)Math.Floor(Position.Y / (tileZ / 2)) / 100000;
                    //+0.00010 is for prevent minus position of y axis that cause z axis bug
                    depth += +0.0001M;
                    //y position on current tile
                    depth += (decimal)((Position.Y % (tileZ / 2)) + 1) / 10000000;
                    depth = Math.Round(depth, 7);
                }

                spriteBatch.Draw(Texture, drawPos, SpriteAnimation.GetSpriteAnimation(), Color * (Calculator.GetPercentage(alpha, 255) / 100.0f),
                    Angle, SpriteCut / 2, Size, SpriteEffects.None, (float)depth);
            }
        }
        /// <summary>
        /// Reactivate and initialize particle to reuse it
        /// </summary>
        /// <param name="texture">Particle Texture</param>
        /// <param name="position">Particle Position</param>
        /// <param name="tileZsize">map tile height, use for calculate y position by z axis </param>
        /// <param name="gravity">Particle gravity</param>
        /// <param name="velocity">Particle velocity</param>
        /// <param name="angle">Particle rotation angle</param>
        /// <param name="angularVelocity">Particle rotation speed</param>
        /// <param name="startColor">Particle color at beginning</param>
        /// <param name="maxColor">Particle color at end</param>
        /// <param name="size">Particle size at start</param>
        /// <param name="maxSize">Particle size at end</param>
        /// <param name="ttl">Particle time to live</param>
        /// <param name="depth">Particle layer depth</param>
        /// <param name="spriteCut">Particle frame size in case particle have more that one frame</param>
        /// <param name="animationTime">Particle animation frame delay</param>
        public void Activate(Texture2D texture, Vector3 position, int tileZsize , Vector3 gravity, Vector3 velocity,
            float angle, float angularVelocity, Vector4 startColor, Vector4 maxColor, float size, float maxSize, float ttl, float depth,
            Vector2 spriteCut, int animationTime)
        {
            Texture = texture;
            Position = position;
            Diraction = velocity;
            Gravity = gravity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            StartColor = startColor;
            MaxColor = maxColor;
            Color = new Color(startColor.X, startColor.Y, startColor.Z, 255);
            currentColor = startColor;
            Size = size;
            MaxTTL = ttl;
            TTL = MaxTTL;
            Depth = depth;
            MaxColor = maxColor;
            MaxSize = maxSize;
            tileZ = tileZsize;

            alpha = startColor.W;

            //calculate color gap
            gapColor = (MaxColor - startColor) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            gapAlpha = (maxColor.W - startColor.W) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));
            //calculate size gap
            gapSize = (MaxSize - size) / (MaxTTL / Calculator.UnitsPerSecound(1.0f));

            //animation
            SpriteAnimation = new Animation(new Rectangle(0, 0, Texture.Width, Texture.Height), spriteCut);
            animateTime = animationTime;
            animateTimeCount = animateTime;
            SpriteCut = spriteCut;

            Active = true;
        }
    }
}
