﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace EarthwormEngine.Utilities
{
    public static class PolygonHelper
    {


        //vector2 helper function
        public static float Vector2CrossProduct(Vector2 a, Vector2 b)
        {
            return (a.X * b.Y) - (a.Y * b.X);
        }

        // The main function that returns true if line segment 'p1q1'
        // and 'p2q2' intersect.
        public static bool IsTwoLineCross(Vector2 p1, Vector2 q1, Vector2 p2, Vector2 q2)
        {
            //check if it dot, what i suppose to do with this??
            if (p1 == q1 || p2 == q2)
                Debugger.Break();

            //if two line is exact the same, return true
            if (p1 == p2 && q1 == q2)
                return true;

            //if two line share 'only' one same point, it not cross
            if ((p1 == p2 && q1 != q2)
                ||(p1 == q2 && q1 != p2)
                ||(q1 == p2 && p1 != q2)
                ||(q1 == q2 && p1 != p2))
                return false;

            // Find the four orientations needed for general and
            // special cases
            int o1 = orientation(p1, q1, p2);
            int o2 = orientation(p1, q1, q2);
            int o3 = orientation(p2, q2, p1);
            int o4 = orientation(p2, q2, q1);

            // General case
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1
            if (o1 == 0 && onSegment(p1, p2, q1)) return true;

            // p1, q1 and q2 are colinear and q2 lies on segment p1q1
            if (o2 == 0 && onSegment(p1, q2, q1)) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2
            if (o3 == 0 && onSegment(p2, p1, q2)) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2
            if (o4 == 0 && onSegment(p2, q1, q2)) return true;

            return false; // Doesn't fall in any of the above cases
        }

        // Given three colinear points p, q, r, the function checks if
        // point q lies on line segment 'pr'
        private static bool onSegment(Vector2 p, Vector2 q, Vector2 r)
        {
            if (q.X <= Math.Max(p.X, r.X) && q.X >= Math.Min(p.X, r.X) &&
                q.Y <= Math.Max(p.Y, r.Y) && q.Y >= Math.Min(p.Y, r.Y))
                return true;

            return false;
        }


        // To find orientation of ordered triplet (p, q, r).
        // The function returns following values
        // 0 --> p, q and r are colinear
        // 1 --> Clockwise
        // 2 --> Counterclockwise
        private static int orientation(Vector2 p, Vector2 q, Vector2 r)
        {
            // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
            // for details of below formula.
            float val = (q.Y - p.Y) * (r.X - q.X) - (q.X - p.X) * (r.Y - q.Y);

            if (val == 0) return 0; // colinear

            return (val > 0) ? 1 : 2; // clock or counterclock wise
        }

        /// <summary>
        /// Triangulate shape from set of vectors (must be ordered in clockwise)
        /// </summary>
        /// <param name="vertices">Shape in list of vectors</param>
        /// <returns>Triangles</returns>
        public static List<List<Vector2>> Triangulate(List<Vector2> vertices)
        {
            if (vertices == null)
                return null;

            if (vertices.Count < 3)
            {
                Debug.WriteLine("Triangulate: shape only has 2 vertices.");
                return null;
            }

            if (vertices.Count > 128)
            {
                Debug.WriteLine("Triangulate: Too complex shape - " + vertices.Count + " vertices.");
                return null;
            }

            //TODO check if vertices is crossed

            //start triangulate process
            List<int> indexList = new List<int>();
            for (int i = 0; i < vertices.Count; i++)
                indexList.Add(i);

            List<List<Vector2>> triangles = new List<List<Vector2>>();

            while (indexList.Count > 3)
            {
                for (int i = 0; i < indexList.Count; i++)
                {
                    Vector2 a = vertices[indexList[i]];

                    int _pre = i - 1;
                    if (_pre < 0)
                        _pre = indexList.Count - 1;
                    Vector2 b = vertices[indexList[_pre]];

                    int _next = i + 1;
                    if (_next >= indexList.Count)
                        _next = 0;
                    Vector2 c = vertices[indexList[_next]];


                    //check if all 3 point are collinear
                    if (IsPointsCollinear(a, b, c))
                    {
                        indexList.RemoveAt(0);
                        continue;
                    }

                    //check if convex vector
                    Vector2 ab = b - a;
                    Vector2 ac = c - a;
                    if (Vector2CrossProduct(ab, ac) < 0.0f)
                    {
                        continue;
                    }

                    bool isEar = true;

                    //check if any vertices is inside the triangle
                    for (int j = 0; j < indexList.Count; j++)
                    {
                        if (vertices[indexList[j]] == a
                            || vertices[indexList[j]] == b
                            || vertices[indexList[j]] == c)
                            continue;

                        if (IsPointInTriangle(b, a, c, vertices[indexList[j]]))
                        {
                            isEar = false;
                            break;
                        }
                    }

                    if (isEar)
                    {
                        //add triangle
                        triangles.Add(new List<Vector2> { b, a, c });

                        //remove vertices from list
                        indexList.RemoveAt(i);
                        break;
                    }
                }
            }

            if (indexList.Count == 3)
            {
                if (IsPointsCollinear(vertices[indexList[0]], vertices[indexList[1]], vertices[indexList[2]]))
                {
                    //colliner
                    //do nothing
                }
                else
                {
                    //not colliner
                    //add triangle
                    triangles.Add(new List<Vector2> { vertices[indexList[0]], vertices[indexList[1]], vertices[indexList[2]] });
                }
            }
            else if (indexList.Count < 3)
            {
                //todo
                //throw new NotImplementedException();
            }

            return triangles;
        }

        public static bool IsPointsCollinear(Vector2 p1, Vector2 p2, Vector2 p3)
        {
            if (p1 == null || p2 == null || p3 == null)
                throw new NotImplementedException();

            if (p1.X * (p2.Y - p3.Y) != 0)
                return false;
            if (p2.X * (p3.Y - p1.Y) != 0)
                return false;
            if (p3.X * (p1.Y - p2.Y) != 0)
                return false;

            return true;
        }

        /// <summary>
        /// Return if point is in triangle, Triangle vertices must be in clockwise order
        /// </summary>
        /// <param name="a">Triangle vertice</param>
        /// <param name="b">Triangle vertice</param>
        /// <param name="c">Triangle vertice</param>
        /// <param name="p">point to check</param>
        /// <returns></returns>
        public static bool IsPointInTriangle(Vector2 a, Vector2 b, Vector2 c, Vector2 p)
        {
            Vector2 ab = b - a;
            Vector2 bc = c - b;
            Vector2 ca = a - c;

            Vector2 ap = p - a;
            Vector2 bp = p - b;
            Vector2 cp = p - c;

            if (Vector2CrossProduct(ab, ap) > 0)
                return false;
            if (Vector2CrossProduct(bc, bp) > 0)
                return false;
            if (Vector2CrossProduct(ca, cp) > 0)
                return false;

            return true;
        }

        public static bool IsShapeInsideShape(List<Vector2> shapeInside, List<Vector2> shapeOutside)
        {
            //check if any edge is crossed
            for (int i = 0; i < shapeInside.Count; i++)
            {
                Vector2 i1 = Vector2.Zero;
                Vector2 i2 = Vector2.Zero;

                Vector2 s1 = Vector2.Zero;
                Vector2 s2 = Vector2.Zero;

                i1 = shapeInside[i];
                //loop back if last vetices
                if (i == shapeInside.Count - 1)
                    i2 = shapeInside[0];
                else
                    i2 = shapeInside[i + 1];

                for (int j = 0; j < shapeOutside.Count; j++)
                {
                    s1 = shapeOutside[j];
                    //loop back if last vetices
                    if (j == shapeOutside.Count - 1)
                        s2 = shapeOutside[0];
                    else
                        s2 = shapeOutside[j + 1];

                    //check if i1,12 is cross s1,s2
                    if (IsTwoLineCross(i1, i2, s1, s2))
                        return false;
                }
            }

            //if all line is not cross
            //check at least one vertices of hole is inside the shape
            Vector2 holePoint = shapeInside[0];
            List<List<Vector2>> shapeOutsideBreaked = Triangulate(shapeOutside);

            for (int i = 0; i < shapeOutsideBreaked.Count; i++)
                if (IsPointInTriangle(shapeOutsideBreaked[i][0], shapeOutsideBreaked[i][1], shapeOutsideBreaked[i][2], holePoint))
                    return true;

            //if point is not in shape return false
            return false;
        }


        /// <summary>
        /// Merge hole polygon into shape
        /// </summary>
        /// <param name="shape">Shape in list of vectors (must be ordered in clockwise)</param>
        /// <param name="hole">Hole in list of vectors (must be ordered in counter-clockwise)</param>
        /// <param name="newShape">New shape in list of vector2</param>
        /// <returns>Is merge successful</returns>
        public static bool MergeHoleIntoShape(List<Vector2> shape, List<Vector2> hole, out List<Vector2> newShape)
        {
            List<Vector2> result = new List<Vector2>();
            result.AddRange(shape);

            //try every possible pairing hole vertices with shape vertices
            List<Pair> pairs = new List<Pair>();
            for (int i = 0; i < hole.Count; i++)
                for (int j = 0; j < shape.Count; j++)
                {
                    //if shape vertices has duplicate, ignore this vertices
                    if (shape.FindAll(v => v == shape[j]).Count == 1)
                        pairs.Add(new Pair(i, j));
                }
            //sort pair for shortest distance
            pairs.Sort((x, y) => Vector2.Distance(hole[x.index1], shape[x.index2]).CompareTo(Vector2.Distance(hole[y.index1], shape[y.index2])));

            //pick pair by order
            for (int i = 0; i < pairs.Count; i++)
            {
                Vector2 p1 = hole[pairs[i].index1];
                Vector2 q1 = shape[pairs[i].index2];

                bool isCrossed = false;

                Vector2 p2;
                Vector2 q2;

                //check if there is any intersection of shape edge
                for (int j = 0; j < shape.Count; j++)
                {
                    p2 = shape[j];
                    q2 = Vector2.Zero;
                    if (j < shape.Count - 1)
                        q2 = shape[j + 1];
                    else
                        q2 = shape[0];

                    if (IsTwoLineCross(p1, q1, p2, q2))
                    {
                        //if cross found, break and check next pairing
                        isCrossed = true;
                        break;
                    }
                }

                //check if there is any intersection of hole edge
                for (int j = 0; j < hole.Count; j++)
                {
                    p2 = hole[j];
                    q2 = Vector2.Zero;
                    if (j < hole.Count - 1)
                        q2 = hole[j + 1];
                    else
                        q2 = hole[0];

                    if (IsTwoLineCross(p1, q1, p2, q2))
                    {
                        //if cross found, break and check next pairing
                        isCrossed = true;
                        break;
                    }
                }

                if (!isCrossed)
                {
                    //if no crossed, try to merge 2 shape together
                    //add shape vertices until hole connection

                    newShape = shape.GetRange(0, pairs[i].index2 + 1);
                    //add hole
                    newShape.AddRange(hole.GetRange(pairs[i].index1, hole.Count - pairs[i].index1));
                    if (pairs[i].index1 > 0)
                        newShape.AddRange(hole.GetRange(0, pairs[i].index1));
                    newShape.Add(hole[pairs[i].index1]);
                    //add the rest of shape
                    if (pairs[i].index2 < shape.Count - 1)
                        newShape.AddRange(shape.GetRange(pairs[i].index2, shape.Count - pairs[i].index2));

                    return true;
                }
                else
                    //if crossed, check next pairing
                    continue;
            }

            newShape = null;
            return false;
        }

        private class Pair
        {
            public int index1;
            public int index2;
            public Pair(int p1Index, int p2Index)
            {
                index1 = p1Index;
                index2 = p2Index;
            }
        }
    }
}
