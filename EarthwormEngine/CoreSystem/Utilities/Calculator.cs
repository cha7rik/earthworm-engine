﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormEngine
{
    /// <summary>
    /// Calculate all things, also include FPS.
    /// </summary>
    public static class Calculator
    {
        private static Random rand;
        private static GameTime gameTime;
        private static Game game;
        /// <summary>
        /// Return average frame per secound.
        /// </summary>
        public static float AverageFPS { get; private set; }
        private static float averageFPSTimeCounter;
        private static uint averageFPSFrameCounter;

        /// <summary>
        /// Initialize calculator, should be called in game initialize.
        /// </summary>
        ///<param name="Game">Current game object.</param>
        public static void Init(Game Game)
        {
            rand = new Random();
            game = Game;
            AverageFPS = 0.0f;
            averageFPSTimeCounter = 0.0f;
            averageFPSFrameCounter = 0;
        }

        /// <summary>
        /// Update calculator, refresh necessary variables.
        /// This should be called in game update, before scene update.
        /// </summary>
        /// ///<param name="GameTime">Current game time.</param>
        public static void Update(GameTime GameTime)
        {
            gameTime = GameTime;
            //calculate aveage fps
            if (averageFPSTimeCounter < 1.0f)
            {
                averageFPSTimeCounter += (float)gameTime.ElapsedGameTime.TotalSeconds;
                averageFPSFrameCounter++;
            }
            else
            {
                AverageFPS = averageFPSFrameCounter / averageFPSTimeCounter;
                averageFPSTimeCounter = 0.0f;
                averageFPSFrameCounter = 0;
            }
        }

        /// <summary>
        /// Return unit per secound calculate by average FPS.
        /// </summary>
        /// <param name="Value">Value</param>
        /// <returns>units per secound</returns>
        public static float UnitsPerSecound(float Value)
        {
            return Value * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        /// <summary>
        /// Return unit per secound calculate by average FPS as vector 2D size.
        /// </summary>
        /// <param name="Value">Value</param>
        /// <returns>Vector 2D size per secound</returns>
        public static Vector2 UnitsPerSecound(Vector2 Value)
        {
            return Value*(float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        /// <summary>
        /// Return unit per secound calculate by average FPS as vector 3D size.
        /// </summary>
        /// <param name="Value">Value</param>
        /// <returns>Vector 3D size per secound</returns>
        public static Vector3 UnitsPerSecound(Vector3 Value)
        {
            return Value * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// Return Vector2 game windows size.
        /// </summary>
        /// <returns>Vector2</returns>
        public static Vector2 GetWnidowsSize()
        {
            return new Vector2(game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);
        }

        /// <summary>
        /// Return Vector2 game screen size.
        /// </summary>
        /// <returns>Vector2</returns>
        public static Vector2 GetScreenSize()
        {
            return ResolutionManager.GetVirtualResolution();
        }

        /// <summary>
        /// Return Vector2 scaling game screen value after screen adjustment.
        /// </summary>
        /// <returns>Vector2</returns>
        public static Vector2 GetScreenScale()
        {
            return new Vector2(ResolutionManager.getScale().X, ResolutionManager.getScale().Y);
        }

        /// <summary>
        /// Convert angle to vector 2D direction.
        /// </summary>
        /// <param name="angle">Angle in radians</param>
        /// <returns>Vector 2D direction</returns>
        public static Vector2 AngleToVector(float angle)
        {
            return new Vector2((float)Math.Sin(angle), -(float)Math.Cos(angle));
        }

        /// <summary>
        /// Convert vector 2D direction to angle.
        /// </summary>
        /// <param name="vector">Vector 2D</param>
        /// <returns>Angle in radians</returns>
        public static float VectorToAngle(Vector2 vector)
        {
            return (float)Math.Atan2(vector.Y, vector.X);
        }

        /// <summary>
        /// Convert angle from degree to radians.
        /// </summary>
        /// <param name="angle">Angle value</param>
        /// <returns>Angle in radians</returns>
        public static float DegreeToRadians(float degree)
        {
            return (float)Math.PI / 180 * degree;
        }

        /// <summary>
        /// Convert angle from radians to degree.
        /// </summary>
        /// <param name="angle">Angle value</param>
        /// <returns>Angle in degree</returns>
        public static float RadiansToDegree(float radian)
        {
            return (float)(radian * (180 / Math.PI));
        }

        /// <summary>
        /// Calculate percentage of two value.
        /// </summary>
        /// <param name="Value">Value</param>
        /// <param name="MaxValue">Max value</param>
        /// <returns>Percentage</returns>
        public static float GetPercentage(float Value, float MaxValue)
        {
            return (Value * 100.0f) / MaxValue;
        }
        /// <summary>
        /// Calculate value form percentage
        /// </summary>
        /// <param name="Value">Value</param>
        /// <param name="Percent">Percentage</param>
        /// <returns>Percentage value</returns>
        public static float GetValueFormPercentage(float Value, float Percent)
        {
            return (Value * Percent) / 100.0f;
        }
        /// <summary>
        /// Return random float value with 2 digits between two value.
        /// </summary>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns>Random float</returns>
        public static float RandomFloat(float min, float max)
        {
            min = min * 100;
            max = max * 100;
            return (float)rand.Next((int)min, (int)max) / 100;
        }

        /// <summary>
        /// Get elapsed game time
        /// </summary>
        /// <returns>Elapsed game time (Seconds)</returns>
        public static double GetElapsedGameTime()
        {
            return gameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// Extract set of number form given value by position and length
        /// </summary>
        /// <param name="number">Set of number to extract</param>
        /// <param name="position">Position of number to extract (count from right)</param>
        /// <param name="length">Length to extract form position</param>
        /// <returns>Set of number in integer format</returns>
        public static int ExtractDigit(int number, uint position, uint length)
        {
            double result = 0;

            result = number / Math.Pow(10, position - 1);
            result = result % Math.Pow(10, length);

            return (int)result;
        }
        /// <summary>
        /// Return linear interpolation point of start and end value by time
        /// </summary>
        /// <param name="StartPoint">Start value</param>
        /// <param name="EndPoint">End value</param>
        /// <param name="Time">Time to plot the point between start and end, must be the value between 0 to 1 : 0 is start point and 1 is end point</param>
        /// <returns>Interpolation point</returns>
        public static float LinearInterpolation(float StartPoint, float EndPoint, float Time)
        {
            return StartPoint+((EndPoint - StartPoint)*Time);
        }

        /// <summary>
        /// Easy convert vector3 to vector2 (dispose Z axis)
        /// </summary>
        /// <param name="vector">Vector3</param>
        /// <returns>Vector2</returns>
        public static Vector2 Vector3ToVector2(Vector3 vector)
        {
            return new Vector2(vector.X, vector.Y);
        }

        /// <summary>
        /// Easy convert vector2 to vector3
        /// </summary>
        /// <param name="vector">Vector2</param>
        /// <param name="z">optional z axis (default is 0)</param>
        /// <returns>Vector3</returns>
        public static Vector3 Vector2ToVector3(Vector2 vector, float z = 0)
        {
            return new Vector3(vector.X, vector.Y, z);
        }
    }
}
