﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EarthwormEngine
{
    /// <summary>
    /// This use for config scene manager, can be inherit to write different draw function
    /// </summary>
    public class SceneManagerConfiguration
    {
        /// <summary>
        /// Game resolution
        /// </summary>
        public Vector2 GameResolution;
        /// <summary>
        /// Window Resolution
        /// </summary>
        public Vector2 WindowResolution;
        /// <summary>
        /// Fullscreen
        /// </summary>
        public bool IsFullScreen;
        /// <summary>
        /// Show mouse cursor
        /// </summary>
        public bool IsMouseVisible;
        /// <summary>
        /// Window title
        /// </summary>
        public string WindowTitle;
        /// <summary>
        /// Determind target platform device for this app
        /// </summary>
        public PlatformDevice ActivePlatform;
        /// <summary>
        /// Set display orientation (only available on mobile platform)
        /// </summary>
        public DisplayOrientation ScreenOrientation;

        /// <summary>
        /// First scene to be load
        /// </summary>
        public CoreScene InitialScene;
        /// <summary>
        /// Enable zipped content and set password for encrypted zip file
        /// </summary>
        public string SetContentZipPassword;
        /// <summary>
        /// Background color;
        /// </summary>
        public Color BackgroundColor;
        /// <summary>
        /// By disable this, FPS will not cap at 60FPS. It may cause FPS drop for some machine but consume less processing
        /// </summary>
        public bool LockFPSLimit;

        public SceneManagerConfiguration()
        {
            //initial config
            GameResolution = new Vector2(960, 640);
            WindowResolution = new Vector2(800, 600);
            IsFullScreen = false;
            IsMouseVisible = false;
            SetContentZipPassword = "";
            WindowTitle = "Window Title";
            ActivePlatform = PlatformDevice.WINDOWS;
            BackgroundColor = Color.Black;
            ScreenOrientation = DisplayOrientation.Unknown;
            LockFPSLimit = false;

            InitialScene = new CoreScene();
        }

        public virtual void Draw(SpriteBatch spriteBatch, CoreScene currentScene)
        {
            ResolutionManager.BeginDraw(BackgroundColor);

            // TODO: Add your drawing code here

            #region Draw using RenderTarget2D (Disable)
            /* DISABLE
            //Pack game screen to renderTarget
            GraphicsDevice.SetRenderTarget(renderTarget);
            GraphicsDevice.Clear(Color.Transparent);

            spriteBatch.Begin(SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend,
                null,
                null,
                null,
                null,
                currentScene.SceneCamera.GetCameraTranformation());

            currentScene.Draw(spriteBatch);
            currentScene.DrawParticle(spriteBatch, false);

            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);

            //Draw game screen as renderTarget
            spriteBatch.Begin();
            spriteBatch.Draw(renderTarget, new Rectangle((int)ResolutionManager.getMarginSize().X, (int)ResolutionManager.getMarginSize().Y, (int)Calculator.GetScreenSize().X, (int)Calculator.GetScreenSize().Y), Color.White);
            spriteBatch.End();
            */
            #endregion 

            #region Normal Draw

            spriteBatch.Begin(SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                currentScene.MainCamera.GetCameraTranformation());

            currentScene.Draw(spriteBatch);
            currentScene.DrawParticle(spriteBatch, false);

            spriteBatch.End();

            #endregion

            //Draw UI
            spriteBatch.Begin(SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                null);
            currentScene.DrawUserInterface(spriteBatch);
            currentScene.DrawParticle(spriteBatch, true);
            currentScene.DrawOverlayColor(spriteBatch);
            spriteBatch.End();
        }
    }
}
