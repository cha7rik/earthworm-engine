﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormEngine.CollisionSystem
{
    /// <summary>
    /// Collision Box class, store collision box data with collision check method
    /// </summary>
    public class CollisionBox
    {
        /// <summary>
        /// Id to identify this collision box (optional)
        /// </summary>
        public int ID = -1;
        /// <summary>
        /// Position of this collision box which use entity's position. Will be (0,0) in case entity is null
        /// </summary>
        public Vector3 Position 
        {
            get 
            {
                if (ent == null)
                    return _position;
                else
                    return ent.Position;
            }
            set 
            {
                if (ent == null)
                    _position = value;
                else
                {
                    Debug.WriteLine("CollisionBox (" + ent.GetType().ToString() + "): Cannot set collision box position that attached with entity");
                    Debugger.Break();
                }
            } 
        }
        private Vector3 _position = new Vector3(0,0,0);
        /// <summary>
        /// Collision Box size
        /// </summary>
        public Vector2 Size { get; set; }
        /// <summary>
        /// Collision Box pivot
        /// </summary>
        public Vector2 Pivot { get; set; }
        /// <summary>
        /// Colliable flag
        /// </summary>
        public bool Collidable { get; set; }
        /// <summary>
        /// The entity that this collision box attached with
        /// </summary>
        public CoreEntity ent { get; protected set; }
        /// <summary>
        /// Tile Y size, use for multiply by z position to project exact positionby Z axis
        /// </summary>
        public float ZaxisSize { get; set; }

        /// <summary>
        /// Create collision box object
        /// </summary>
        /// <param name="entity">The entity that this collision box attached with</param>
        /// <param name="size">collision box size</param>
        /// <param name="pivot">collision box pivot</param>
        /// <param name="isCollidable">collidable flag</param>
        /// <param name="zAxisSize">z axis size for 3d collision (optional, default is 1)</param>
        public CollisionBox(CoreEntity entity, Vector2 size, Vector2 pivot, bool isCollidable, float zAxisSize = 1)
        {
            Size = size;
            Pivot = pivot;
            Collidable = isCollidable;
            ent = entity;
            ZaxisSize = zAxisSize;
        }

        /// <summary>
        /// Create collision box object without entity
        /// </summary>
        /// <param name="position">collidion box position</param>
        /// <param name="size">collision box size</param>
        /// <param name="pivot">collision box pivot</param>
        /// <param name="isCollidable">collidable flag</param>
        /// <param name="zAxisSize">z axis size for 3d collision (optional, default is 1)</param>
        public CollisionBox(Vector3 position, Vector2 size, Vector2 pivot, bool isCollidable, float zAxisSize = 1)
        {
            ent = null;
            Position = position;
            Size = size;
            Pivot = pivot;
            Collidable = isCollidable;
            ZaxisSize = zAxisSize;
        }

        /// <summary>
        /// Check collides for two collision box
        /// </summary>
        /// <param name="otherCollisionBox">Target collision box to be checked</param>
        /// <returns>Is collide</returns>
        public bool Collide(CollisionBox otherCollisionBox)
        {
            if (!otherCollisionBox.Collidable || !Collidable || otherCollisionBox == null) return false;
            if (Position.Z != otherCollisionBox.Position.Z) return false;
            if (Position.X - Pivot.X + Size.X <= otherCollisionBox.Position.X - otherCollisionBox.Pivot.X) return false;
            if (Position.Y - Pivot.Y + Size.Y <= otherCollisionBox.Position.Y - otherCollisionBox.Pivot.Y) return false;
            if (Position.X - Pivot.X >= otherCollisionBox.Position.X - otherCollisionBox.Pivot.X + otherCollisionBox.Size.X) return false;
            if (Position.Y - Pivot.Y >= otherCollisionBox.Position.Y - otherCollisionBox.Pivot.Y + otherCollisionBox.Size.Y) return false;
            return true;
        }

        /// <summary>
        /// Check collition on specific position (will not count z axis for point)
        /// </summary>
        /// <param name="point">Position to check</param>
        /// <returns></returns>
        public bool Collide(Vector3 point)
        {
            if (!Collidable) return false;
            float zSize = ZaxisSize * Position.Z;
            if (Position.X - Pivot.X >= point.X) return false;
            if (Position.Y - Pivot.Y - zSize >= point.Y) return false;
            if (Position.X - Pivot.X + Size.X <= point.X) return false;
            if (Position.Y - Pivot.Y + Size.Y - zSize <= point.Y) return false;
            return true;

        }
    }
}
