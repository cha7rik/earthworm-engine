﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using EarthwormEngine.Utilities;

namespace EarthwormEngine.CollisionSystem
{
    public class SATCollisionBox
    {
        /// <summary>
        /// Id to identify this collision box (optional)
        /// </summary>
        public int ID = 1;
        public List<List<Vector2>> Triangles { get; protected set; }
        public bool Collidable;
        public CoreEntity ent;
        /// <summary>
        /// Tile Y size, use for multiply by z position to project exact positionby Z axis
        /// </summary>
        public float ZaxisSize;

        public SATCollisionBox()
        {
            Triangles = new List<List<Vector2>>();
            Collidable = false;
            ent = null;
            ZaxisSize = 0;
        }

        /// <summary>
        /// Initialize SAT collision box
        /// </summary>
        /// <param name="entity">Entity of this collsion box</param>
        /// <param name="listOfShapeEdge">Vertices in clockwise order</param>
        /// <param name="zAxisSize">Z size</param>
        /// <param name="isCollidable">Collidable flag</param>
        public SATCollisionBox(CoreEntity entity, List<Vector2> listOfShapeEdge, float zAxisSize, bool isCollidable)
        {
            this.ent = entity;
            Collidable = isCollidable;
            ZaxisSize = zAxisSize;

            Triangles = new List<List<Vector2>>();
            //try break the shape into triangles
            Triangles = PolygonHelper.Triangulate(listOfShapeEdge);
        }

        /// <summary>
        /// Initialize SAT collision box with hole in it
        /// </summary>
        /// <param name="entity">Entity of this collsion box</param>
        /// <param name="listOfShapeEdge">Vertices in clockwise order</param>
        /// <param name="holes">Vertices of holes in counter-clockwise order</param>
        /// <param name="zAxisSize">Z size</param>
        /// <param name="isCollidable">Collidable flag</param>
        public SATCollisionBox(CoreEntity entity, List<Vector2> listOfShapeEdge, List<List<Vector2>> holes, float zAxisSize, bool isCollidable)
        {
            this.ent = entity;
            Collidable = isCollidable;
            ZaxisSize = zAxisSize;

            //merge hole into shape
            for (int i = 0; i < holes.Count; i++)
            {
                List<Vector2> newShape = null;
                if (PolygonHelper.MergeHoleIntoShape(listOfShapeEdge, holes[i], out newShape))
                    listOfShapeEdge = newShape;
                else
                {
                    Debug.WriteLine("Hole cannot be add to shape. \nShape");
                    for (int j = 0; j < listOfShapeEdge.Count; j++)
                        Debug.WriteLine("(" + listOfShapeEdge[j].X + "," + listOfShapeEdge[j].Y + ")");
                    Debug.WriteLine("Hole");
                    for (int j = 0; j < holes[i].Count; j++)
                        Debug.WriteLine("(" + holes[i][j].X + "," + holes[i][j].Y + ")");
                    Debug.WriteLine("-----");
                    throw new InvalidOperationException("Hole cannot be add to shape.");
                }
            }

            Triangles = new List<List<Vector2>>();
            //try break the shape into triangles
            Triangles = PolygonHelper.Triangulate(listOfShapeEdge);
        }

        /// <summary>
        /// Check collides for two SAT collision box
        /// </summary>
        /// <param name="other">Other collision box</param>
        /// <param name="MVT">Minimum Translation Vector, return -1 if not collide (not implemented)</param>
        /// <returns>Is two shape collide</returns>
        public bool Collide(SATCollisionBox other, out Vector2 MVT)
        {
            //null handler
            Vector3 entPos = Vector3.Zero;
            if (ent != null)
                entPos = ent.Position;
            Vector3 otherEntPos = Vector3.Zero;
            if (other.ent != null)
                otherEntPos = other.ent.Position;


            MVT = Vector2.Zero;
            if (!other.Collidable || !Collidable)
                return false;
            if (otherEntPos.Z != entPos.Z) return false;

            if (Triangles.Count == 0)
                return false;

            for (int i = 0; i < Triangles.Count; i++)
            {
                //check all triangles vs other triangles
                for (int j = 0; j < other.Triangles.Count; j++)
                    if (SATCollide(Triangles[i], new Vector2(otherEntPos.X, otherEntPos.Y), other.Triangles[j], out MVT))
                        return true;
            }

            return false;

        }
        /// <summary>
        /// Check collides for SAT collision box and AABB collision box
        /// </summary>
        /// <param name="other">Other AABB collision box</param>
        /// <param name="MVT">Minimum Translation Vector, return -1 if not collide (not implemented)</param>
        /// <returns>Is two shape collide</returns>
        public bool Collide(CollisionBox other, out Vector2 MVT)
        {
            MVT = Vector2.Zero;

            //null handler
            Vector3 entPos = Vector3.Zero;
            if (ent != null)
                entPos = ent.Position;
            Vector3 otherEntPos = Vector3.Zero;
            if (other.ent != null)
                otherEntPos = other.ent.Position;

            if (!other.Collidable || !Collidable)
                return false;

            if (otherEntPos.Z != entPos.Z) return false;

            List<Vector2> otherVertices = new List<Vector2>();
            otherVertices.Add(other.Pivot * -1);
            otherVertices.Add(new Vector2(other.Size.X - other.Pivot.X, other.Pivot.Y * -1));
            otherVertices.Add(new Vector2(other.Pivot.Y * -1, other.Size.Y - other.Pivot.Y));
            otherVertices.Add(new Vector2(other.Size.X - other.Pivot.X, other.Size.Y - other.Pivot.Y));

            for (int i = 0; i < Triangles.Count; i++)
                if (SATCollide(Triangles[i], new Vector2(otherEntPos.X, otherEntPos.Y), otherVertices, out MVT))
                    return true;

            return false;
        }
        /// <summary>
        /// Check collides for specific point (this method will return result regardless collidable or not)
        /// </summary>
        /// <param name="point">Position to check</param>
        /// <param name="MVT">Minimum Translation Vector, return -1 if not collide (not implemented)</param>
        /// <returns>Is shape collide with point</returns>
        public bool Collide(Vector3 point, out Vector2 MVT)
        {
            //null handler
            Vector3 entPos = Vector3.Zero;
            if (ent != null)
                entPos = ent.Position;

            Vector2 newYPos = new Vector2(0, entPos.Z * ZaxisSize);
            List<Vector2> otherVertices = new List<Vector2>();
            otherVertices.Add(new Vector2(0f, -0.5f) + newYPos);
            otherVertices.Add(new Vector2(1f, 0f) + newYPos);
            otherVertices.Add(new Vector2(-1f, 0f) + newYPos);

            for (int i = 0; i < Triangles.Count; i++)
                if (SATCollide(Triangles[i], Calculator.Vector3ToVector2(point), otherVertices, out MVT))
                    return true;

            MVT = Vector2.Zero;
            return false;
        }

        //collision check method
        private bool SATCollide(List<Vector2> thisVertices, Vector2 otherPosition, List<Vector2> otherVertices, out Vector2 MVT)
        {
            bool result = true;

            MVT = new Vector2(-1, -1);
            List<Vector2> Axis = new List<Vector2>();

            //exit if one of polygon has no edge
            if (thisVertices.Count <= 0 || otherVertices.Count <= 0)
                return false;

            //initialize distance
            float minIntervalDistance = float.MaxValue;

            Vector2 translationAxis = Vector2.Zero;
            //find edge from vertices of both polygon
            List<Vector2> edges = new List<Vector2>();
            for (int i = 0; i < thisVertices.Count; i++)
            {
                //check if not the last vertices
                if (i != thisVertices.Count - 1)
                    edges.Add(thisVertices[i + 1] - thisVertices[i]);
                else
                {
                    //if last vertices, loop edge back
                    edges.Add(thisVertices[0] - thisVertices[i]);
                }
            }
            for (int i = 0; i < otherVertices.Count; i++)
            {
                //check if not the last vertices
                if (i != otherVertices.Count - 1)
                    edges.Add(otherVertices[i + 1] - otherVertices[i]);
                else
                {
                    //if last vertices, loop edge back
                    edges.Add(otherVertices[0] - otherVertices[i]);
                }
            }
            //loop through all edges of both polygon
            for (int i = 0; i < edges.Count; i++)
            {
                //find if polygon currently intersecting
                //find axis prependicular to current edge
                Vector2 axis = Vector2.Normalize(new Vector2(edges[i].Y * -1, edges[i].X));

                //find projection of the polygon on the current axis
                float minA = 0;
                float minB = 0;
                float maxA = 0;
                float maxB = 0;

                //null handler
                Vector2 entPos = Vector2.Zero;
                if (ent != null)
                    entPos = new Vector2(ent.Position.X, ent.Position.Y);

                ProjectPolygon(axis, thisVertices, out minA, out maxA, new Vector2(entPos.X, entPos.Y));
                ProjectPolygon(axis, otherVertices, out minB, out maxB, otherPosition);
                //check if projection is intersect, if not, break the loop
                float intervalDistance = IntervalDistance(minA, maxA, minB, maxB);
                if (intervalDistance > 0)
                {
                    result = false;
                    break;
                }

                /* ===== 2. Now find if the polygons *will* intersect =====

                // Project the velocity on the current axis
                float velocityProjection = axis.DotProduct(velocity);

                // Get the projection of polygon A during the movement
                if (velocityProjection < 0)
                {
                    minA += velocityProjection;
                }
                else
                {
                    maxA += velocityProjection;
                }

                // Do the same test as above for the new projection
                float intervalDistance = IntervalDistance(minA, maxA, minB, maxB);
                if (intervalDistance > 0) result.WillIntersect = false;

                // If the polygons are not intersecting and won't intersect, exit the loop
                if (!result.Intersect && !result.WillIntersect) break;

                // Check if the current interval distance is the minimum one. If so store
                // the interval distance and the current distance.
                // This will be used to calculate the minimum translation vector
                intervalDistance = Math.Abs(intervalDistance);
                if (intervalDistance < minIntervalDistance)
                {
                    minIntervalDistance = intervalDistance;
                    translationAxis = axis;

                    Vector d = polygonA.Center - polygonB.Center;
                    if (d.DotProduct(translationAxis) < 0)
                        translationAxis = -translationAxis;
                }
            }

            // The minimum translation vector
            // can be used to push the polygons appart.
            if (result.WillIntersect)
                result.MinimumTranslationVector =
                       translationAxis * minIntervalDistance;
            https://www.codeproject.com/Articles/15573/2D-Polygon-Collision-Detection
            */

                //TODO:calculate MVT

            }


            return result;
        }

        //calculate projection of box given on axis
        private void ProjectPolygon(Vector2 axis, List<Vector2> vertices, out float min, out float max, Vector2 position)
        {
            //project point on axis with dot product, start with the first vertices of the box
            float dotProduct = Vector2.Dot(axis, vertices[0] + position);
            min = dotProduct;
            max = dotProduct;

            //start project for all vertices, this will return min vertices and max vertices projection on axis
            for (int i = 0; i < vertices.Count; i++)
            {
                dotProduct = Vector2.Dot(vertices[i] + position, axis);
                if (dotProduct < min)
                    min = dotProduct;
                if (dotProduct > max)
                    max = dotProduct;
            }
        }

        //return distance between two projection
        private float IntervalDistance(float minA, float maxA, float minB, float maxB)
        {
            if (minA < minB)
                return minB - maxA;
            else
                return minA - maxB;
        }
    }
}
