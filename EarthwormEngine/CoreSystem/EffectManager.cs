﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using EarthwormEngine.ParticleSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// Class that manage all particle effect in scene
    /// </summary>
    public class EffectManager
    {
        /// <summary>
        /// List of particle effect this class contain
        /// </summary>
        public List<ParticleEffect> ParticleEffectArray;
        private List<Particle> ParticleBucket;
        public SceneManager sceneManager;
        /// <summary>
        /// Paticle texture path within content file
        /// </summary>
        public string ParticleTexturePath { get; private set; }

        private uint particleIDrun;

        /// <summary>
        /// Create effect manager
        /// </summary>
        /// <param name="sm">SceneManager</param>
        public EffectManager(SceneManager sm)
        {
            sceneManager = sm;
            particleIDrun = 0;;
            //set default path
            ParticleTexturePath = "particles";
        }
        /// <summary>
        /// Initialize effect manager, should be called in scene initialize method
        /// </summary>
        public void Init()
        {
            ParticleEffectArray = new List<ParticleEffect>();
            ParticleBucket = new List<Particle>();
        }
        /// <summary>
        /// Update effect manager and all particle effects in array, should be called in scene update method
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                ParticleEffectArray[i].Update();
                //remove all unrepeat particle that run out
                if (!ParticleEffectArray[i].isRepeat)
                {
                    if (ParticleEffectArray[i].particleCreated >= ParticleEffectArray[i].MaximumParticle
                        && ParticleEffectArray[i].GetParticleAliveCount() <= 0
                        && !ParticleEffectArray[i].isRepeat)
                    {
                        ParticleEffectArray[i].Kill();
                        ParticleEffectArray.Remove(ParticleEffectArray[i]);
                        continue;
                    }
                    if (ParticleEffectArray[i].IsKilled()
                    && ParticleEffectArray[i].GetActiveParticle() <= 0)
                    {
                        ParticleEffectArray[i].Kill();
                        ParticleEffectArray.Remove(ParticleEffectArray[i]);
                        continue;
                    }
                }
            }
        }
        /// <summary>
        /// Draw all particle effects, should be called in scene draw method
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        /// <param name="cameraViewPort">Viewport of current scene camera, use for optimize</param>
        /// <param name="isUIParticle">Draw ui particle or not</param>
        public void Draw(SpriteBatch spriteBatch, Rectangle cameraViewPort, bool isUIParticle)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                if (isUIParticle == ParticleEffectArray[i].isUiParticle)
                {
                    if (isUIParticle)
                        ParticleEffectArray[i].Draw(spriteBatch);
                    else
                        ParticleEffectArray[i].Draw(spriteBatch, cameraViewPort);
                }
            }
        }
        /// <summary>
        /// Add new particle effect
        /// </summary>
        /// <param name="particle">Particle effect that prefer to added</param>
        /// <param name="isUIParticle">Is create particle on UI layer</param>
        public void Add(ParticleEffect particle, bool isUIParticle)
        {
            particle.isUiParticle = isUIParticle;
            ParticleEffectArray.Add(particle);
            particle.ID = (int)particleIDrun;
            particle.Load(sceneManager.GetContentManager());
            particleIDrun++;
        }

        /// <summary>
        /// Add new particle effect by template
        /// </summary>
        /// <param name="path">Template file path (if in Content folder, must be also add "Content\")</param>
        /// <param name="position">Vector 3D particle emit position</param>
        /// <param name="isUIParticle">Is create particle on UI layer</param>
        public void Add(string path, Vector2 position, bool isUIParticle)
        {
            ParticleEffect particle = new ParticleEffect(path, this, sceneManager.GetContentManager());
            particle.isUiParticle = isUIParticle;
            particle.EmitPosition = new Vector3(position.X, position.Y, 0);
            ParticleEffectArray.Add(particle);
            particle.ID = (int)particleIDrun;
            particle.Load(sceneManager.GetContentManager());
            particleIDrun++;
        }

        /// <summary>
        /// Add new particle effect by template in 3D element
        /// </summary>
        /// <param name="path">Template file path (if in Content folder, must be also add "Content\")</param>
        /// <param name="position">Vector 3D particle emit position</param>
        /// <param name="isUIParticle">Is create particle on UI layer</param>
        public void Add(string path, Vector3 position, bool isUIParticle)
        {
            ParticleEffect particle = new ParticleEffect(path, this, sceneManager.GetContentManager());
            particle.isUiParticle = isUIParticle;
            particle.EmitPosition = position;
            ParticleEffectArray.Add(particle);
            particle.ID = (int)particleIDrun;
            particle.Load(sceneManager.GetContentManager());
            particleIDrun++;
        }

        /// <summary>
        /// Seek particle effect in array by ID
        /// </summary>
        /// <param name="ID">Particle effect ID</param>
        /// <returns>Particle effect (return null if not found)</returns>
        public ParticleEffect Get(int ID)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                if (ParticleEffectArray[i].ID == ID)
                    return ParticleEffectArray[i];
            }
            return null;
        }
        /// <summary>
        /// Remove particle effect from array
        /// </summary>
        /// <param name="ID">Particle effect ID</param>
        public void Remove(int ID)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                if (ParticleEffectArray[i].ID == ID)
                {
                    ParticleEffectArray[i].RemoveAllParticle();
                    ParticleEffectArray.Remove(ParticleEffectArray[i]);
                }
            }
        }

        /// <summary>
        /// Remove all particle effects in array
        /// </summary>
        public void RemoveAll()
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                ParticleEffectArray[i].RemoveAllParticle();
                ParticleEffectArray.Remove(ParticleEffectArray[i]);
            }
        }

        /// <summary>
        /// Get an inactive particle
        /// </summary>
        /// <returns>an inactive particle</returns>
        public Particle GetInactiveParticle()
        {
            Particle par = null;
            for (int i = 0; i < ParticleBucket.Count; i++)
            {
                if (!ParticleBucket[i].Active)
                {
                    par = ParticleBucket[i];
                    break;
                }
            }
            if (par == null)
            {
                par = new Particle();
                ParticleBucket.Add(par);
            }
            return par;
        }

        /// <summary>
        /// Set particle texture path
        /// </summary>
        /// <param name="newPath">File path within content</param>
        public void SetPaticleTexturePath(string newPath)
        {
            ParticleTexturePath = newPath;
        }
    }
}
