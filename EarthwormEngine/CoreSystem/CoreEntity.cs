﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using EarthwormEngine.CollisionSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// Entity class for game, could be charater, item, bullet, etc.
    /// </summary>
    public abstract class CoreEntity
    {
        /// <summary>
        /// Custom id to identify this entity (optional)
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// The scene this entity is.
        /// </summary>
        public CoreScene activeScene { set; get; }
        //general
        /// <summary>
        /// Entity ID, connot be duplicated
        /// </summary>
        public uint EntityID { get; private set; }
        /// <summary>
        /// Custom name for this entity, can be called
        /// </summary>
        public string EntityName { get; set; }
        /// <summary>
        /// Entity's position
        /// </summary>
        public Vector3 Position;
        /// <summary>
        /// Entity's position in Vector 2D format
        /// </summary>
        public Vector2 PositionXY { get { return new Vector2(Position.X, Position.Y); } private set { Position = new Vector3(value.X, value.Y, Position.Z); } }
        /// <summary>
        /// Rotation angle for this entity
        /// Default is 0.0f
        /// </summary>
        public float Angle;
        /// <summary>
        /// Entity face direction
        /// Default is FaceDirection.DOWN
        /// </summary>
        public FaceDirection FaceDirection { get; set; }
        /// <summary>
        /// Static flag for this entity (unmovable)
        /// </summary>
        public bool StaticEntity { get; set; }
        /// <summary>
        /// Direction that this entity face to as vector 2D
        /// </summary>
        public Vector3 MoveDirection;
        //sprite
        /// <summary>
        /// Name of this entity's texture
        /// Default is ""
        /// </summary>
        public string TextureName { get; set; }
        /// <summary>
        /// Texture for this entity
        /// </summary>
        protected Texture2D Texture;
        /// <summary>
        /// This entity's pivot
        /// Default is (0,0)
        /// </summary>
        public Vector2 Pivot { get; set; }
        /// <summary>
        /// Layer depth for draw entity sprite
        /// </summary>
        public float LayerDepth { get; set; }
        /// <summary>
        /// Scale size of this entity
        /// Default is 1.0f
        /// </summary>
        public Vector2 Scale;
        /// <summary>
        /// Frame size for this entity in case it has more than one frame
        /// </summary>
        public Rectangle SpriteCut { get; set; }
        /// <summary>
        /// Entity's color blending
        /// </summary>
        public Color EntityColor { get; set; }
        /// <summary>
        /// Entity's opacity
        /// </summary>
        public float EntityOpacity;
        /// <summary>
        /// Animation class for this entity
        /// </summary>
        public Animation SpriteAnimation;

        //collision
        /// <summary>
        /// CollisionBox object for this entity
        /// </summary>
        public List<CollisionBox> collisionBox { get; set; }
        /// <summary>
        /// collision box for SAT
        /// </summary>
        public List<SATCollisionBox> SATcollisionBox { get; set; }

        //draw collision box for debug
        protected bool isDrawCollisionBox = false;
        protected Texture2D collisionBoxTexture;
        protected Rectangle collisionBoxRectangle;
        protected Color collisionBoxColor = Color.White;

        /// <summary>
        /// Enable entity (update, draw)
        /// </summary>
        public bool Enable = true;

        /// <summary>
        /// Is draw this entity texture
        /// </summary>
        public bool Visible = true;

        /// <summary>
        /// Flip entity texture horizontally
        /// </summary>
        public bool FlipHorizontal = false;

        /// <summary>
        /// Flip entity texture vertically
        /// </summary>
        public bool FlipVertical = false;

        /// <summary>
        /// Flag as unused entity to release all reference (read only)
        /// </summary>
        public bool Disposed { private set; get; }

        /// <summary>
        /// Contain custom properties form tile map editor
        /// </summary>
        public string CustomProperties = string.Empty;

        /// <summary>
        /// Create entity
        /// </summary>
        public CoreEntity()
        {
            //general
            EntityID = uint.MaxValue;
            EntityName = "";
            Position = Vector3.Zero;
            Angle = 0.0f;
            FaceDirection = FaceDirection.Down;
            StaticEntity = false;
            MoveDirection = Vector3.Zero;
            //sprite
            TextureName = "";
            Pivot = Vector2.Zero;
            Scale = new Vector2(1.0f, 1.0f);
            EntityColor = Color.White;
            EntityOpacity = 1.0f;
            LayerDepth = 0.5f;
            //collision system
            collisionBox = new List<CollisionBox>();
            SATcollisionBox = new List<SATCollisionBox>();
        }

        /// <summary>
        /// Initialize entity's properties, should be called in scene initialize method
        /// </summary>
        public virtual void Init()
        {
            
        }

        /// <summary>
        /// Load entity's content (spritesheet, etc.) should be called in scene load method
        /// </summary>
        /// <param name="contentManager">SceneManager's ContentManager</param>
        public virtual void Load(IContentManager contentManager)
        {
            if (!string.IsNullOrEmpty(TextureName) && Texture == null)
            {
                Texture = contentManager.Load<Texture2D>(TextureName);
                //to prevent blank image and visual glitch, set spriteCut as texture size if spriteCut is not initiate
                if (SpriteCut == Rectangle.Empty)
                    SpriteCut = new Rectangle(0, 0, Texture.Width, Texture.Height);
                SpriteAnimation = new Animation(new Rectangle(0, 0, Texture.Width, Texture.Height), new Vector2(SpriteCut.Width, SpriteCut.Height));
            }

            if (isDrawCollisionBox && (collisionBox.Count > 0 || SATcollisionBox.Count > 0))
            {
                collisionBoxTexture = new Texture2D(activeScene.sceneManager.GraphicsDevice, 1, 1);
                collisionBoxTexture.SetData(new Color[] { Color.White });
            }
        }

        /// <summary>
        /// Update logic for entity, should be called in scene update method
        /// </summary>
        public virtual void Update()
        {

        }

        /// <summary>
        /// Draw this entity sprite, should be called in scene draw method
        /// </summary>
        /// <param name="spriteBatch">SceneManager's SpriteBatch</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            Debug.WriteLine("[WARNING] Entity.Draw(SpriteBatch) : Method is outdated, isometric and 2.5D is not supported");
            if (Enable && Visible)
            {
                if (!string.IsNullOrEmpty(TextureName))
                {
                    SpriteEffects flipHorizotally = SpriteEffects.FlipHorizontally;
                    if (!FlipHorizontal)
                        flipHorizotally = SpriteEffects.None;
                    SpriteEffects flipVertically = SpriteEffects.FlipVertically;
                    if (!FlipVertical)
                        flipVertically = SpriteEffects.None;

                    spriteBatch.Draw(Texture, new Vector2(Position.X,Position.Y), SpriteAnimation.GetSpriteAnimation(), EntityColor * EntityOpacity, Calculator.DegreeToRadians(Angle), Pivot, Scale, flipHorizotally | flipVertically, LayerDepth);
                }

                if (isDrawCollisionBox && collisionBox.Count > 0)
                {
                    for (int i = 0; i < collisionBox.Count; i++)
                    {
                        collisionBoxRectangle = new Rectangle((int)(Position.X - collisionBox[i].Pivot.X), (int)(Position.Y - collisionBox[i].Pivot.Y), (int)collisionBox[i].Size.X, (int)collisionBox[i].Size.Y);
                        spriteBatch.Draw(collisionBoxTexture, collisionBoxRectangle, null, collisionBoxColor * 0.5f, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
                    }
                    for (int i = 0; i < SATcollisionBox.Count; i++)
                    {
                        for (int j = 0; j < SATcollisionBox.Count; j++) 
                        {
                            float distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][0], SATcollisionBox[i].Triangles[j][1]);
                            float angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][1].Y - SATcollisionBox[i].Triangles[j][0].Y, SATcollisionBox[i].Triangles[j][1].X - SATcollisionBox[i].Triangles[j][0].X);
                            spriteBatch.Draw(collisionBoxTexture, SATcollisionBox[i].Triangles[j][0], null, collisionBoxColor * 0.5f, angle, new Vector2(0,0.5f), new Vector2(distance, 1) , SpriteEffects.None, 1.0f);
                            distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][1], SATcollisionBox[i].Triangles[j][2]);
                            angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][2].Y - SATcollisionBox[i].Triangles[j][1].Y, SATcollisionBox[i].Triangles[j][2].X - SATcollisionBox[i].Triangles[j][1].X);
                            spriteBatch.Draw(collisionBoxTexture, SATcollisionBox[i].Triangles[j][1], null, collisionBoxColor * 0.5f, angle, new Vector2(0, 0.5f), new Vector2(distance, 1), SpriteEffects.None, 1.0f);
                            distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][2], SATcollisionBox[i].Triangles[j][0]);
                            angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][0].Y - SATcollisionBox[i].Triangles[j][2].Y, SATcollisionBox[i].Triangles[j][0].X - SATcollisionBox[i].Triangles[j][2].X);
                            spriteBatch.Draw(collisionBoxTexture, SATcollisionBox[i].Triangles[j][0], null, collisionBoxColor * 0.5f, angle, new Vector2(0, 0.5f), new Vector2(distance, 1), SpriteEffects.None, 1.0f);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Draw this entity sprite, should be called in scene draw method
        /// </summary>
        /// <param name="spriteBatch">SceneManager's SpriteBatch</param>
        /// <param name="cameraViewPort">For optimize : disable drawing if out of camera viewport</param>
        public virtual void Draw(SpriteBatch spriteBatch, Rectangle cameraViewPort, MapOrientation tileOrientation, float TileHeight)
        {
            if (Enable && Visible)
            {
                if (!string.IsNullOrEmpty(TextureName))
                {
                    int tileSizeY = 0;
                    //default depth is 0.4
                    float depth = 0.0f;
                    //custom depth
                    depth += LayerDepth / 100000f;

                    //calculate draw position by z
                    Vector2 drawPos = new Vector2(Position.X, Position.Y);

                    //calculate depth for isomectic map
                    if (tileOrientation == MapOrientation.Isometric)
                    {
                        if (Texture != null)
                        {
                            if(activeScene.tiledMap != null)
                                tileSizeY = (int)activeScene.tiledMap.girdSize.Y;

                            drawPos = new Vector2(Position.X, Position.Y - (Position.Z * tileSizeY));
                        }
                    }

                    SpriteEffects flipHorizotally = SpriteEffects.FlipHorizontally;
                    if (!FlipHorizontal)
                        flipHorizotally = SpriteEffects.None;
                    SpriteEffects flipVertically = SpriteEffects.FlipVertically;
                    if (!FlipVertical)
                        flipVertically = SpriteEffects.None;

                    //not draw if out of viewport
                    if (drawPos.X + (SpriteCut.Width - Pivot.X) * Scale.X >= cameraViewPort.X
                        && drawPos.Y + (SpriteCut.Height - Pivot.Y) * Scale.Y >= cameraViewPort.Y
                        && drawPos.X - (Pivot.X * Scale.X) <= cameraViewPort.Width
                        && drawPos.Y - (Pivot.Y * Scale.Y) <= cameraViewPort.Height)
                    {

                        if (tileOrientation == MapOrientation.Isometric)
                        {
                            //calculate y axis layer by tile height
                            depth += (float)Math.Floor(Position.Y / (tileSizeY / 2)) / 1000;
                            //+0.0010 is for prevent minus position of y axis that cause z axis bug
                            depth += +0.01f;
                            //y position on current tile
                            depth += ((Position.Y % (tileSizeY / 2)) + 1) / 100000;
                            depth = (float)Math.Round(depth, 7);
                        }

                        spriteBatch.Draw(Texture, drawPos, SpriteAnimation.GetSpriteAnimation(), EntityColor * EntityOpacity, Calculator.DegreeToRadians(Angle), Pivot, Scale, flipHorizotally | flipVertically, (float)depth);
                    }
                }

                if (isDrawCollisionBox && (collisionBox.Count > 0 || SATcollisionBox.Count > 0))
                {
                    for (int i = 0; i < collisionBox.Count; i++)
                    {
                        collisionBoxRectangle = new Rectangle((int)(Position.X - collisionBox[i].Pivot.X), (int)(Position.Y - collisionBox[i].Pivot.Y), (int)collisionBox[i].Size.X, (int)collisionBox[i].Size.Y);
                        spriteBatch.Draw(collisionBoxTexture, collisionBoxRectangle, null, collisionBoxColor * 0.5f, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
                    }
                    for (int i = 0; i < SATcollisionBox.Count; i++)
                    {
                        for (int j = 0; j < SATcollisionBox[i].Triangles.Count; j++)
                        {
                            float distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][0], SATcollisionBox[i].Triangles[j][1]);
                            float angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][1].Y - SATcollisionBox[i].Triangles[j][0].Y, SATcollisionBox[i].Triangles[j][1].X - SATcollisionBox[i].Triangles[j][0].X);
                            spriteBatch.Draw(collisionBoxTexture, PositionXY + SATcollisionBox[i].Triangles[j][0], null, collisionBoxColor * 0.5f, angle, new Vector2(0, 0.5f), new Vector2(distance, 1), SpriteEffects.None, 1.0f);
                            distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][1], SATcollisionBox[i].Triangles[j][2]);
                            angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][2].Y - SATcollisionBox[i].Triangles[j][1].Y, SATcollisionBox[i].Triangles[j][2].X - SATcollisionBox[i].Triangles[j][1].X);
                            spriteBatch.Draw(collisionBoxTexture, PositionXY + SATcollisionBox[i].Triangles[j][1], null, collisionBoxColor * 0.5f, angle, new Vector2(0, 0.5f), new Vector2(distance, 1), SpriteEffects.None, 1.0f);
                            distance = Vector2.Distance(SATcollisionBox[i].Triangles[j][2], SATcollisionBox[i].Triangles[j][0]);
                            angle = (float)Math.Atan2(SATcollisionBox[i].Triangles[j][0].Y - SATcollisionBox[i].Triangles[j][2].Y, SATcollisionBox[i].Triangles[j][0].X - SATcollisionBox[i].Triangles[j][2].X);
                            spriteBatch.Draw(collisionBoxTexture, PositionXY + SATcollisionBox[i].Triangles[j][0], null, collisionBoxColor * 0.5f, angle, new Vector2(0, 0.5f), new Vector2(distance, 1), SpriteEffects.None, 1.0f);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unload this entity content, should be called in scene unload method
        /// </summary>
        public virtual void Unload()
        {

        }

        /// <summary>
        /// Delete this entity and kill all particle effect
        /// </summary>
        public virtual void DeleteEntity()
        {
            Disposed = true;
        }
        /// <summary>
        /// Get entity array that this entity belong
        /// </summary>
        /// <returns>Entity array</returns>
        protected List<CoreEntity> GetSceneEntityList()
        {
            return activeScene.GetSceneEntityList();
        }

        /// <summary>
        /// Check if this entity collide with specific entity (SAT collision supported)
        /// </summary>
        /// <param name="entity">Another entity to check</param>
        /// <returns>return true if collide</returns>
        public bool Collide(CoreEntity entity)
        {
            if (this.EntityID == entity.EntityID)
            {
                //checking collision with itself, something must be wrong
                Debugger.Break();
                return false;
            }

            //no collision box
            if (collisionBox.Count <= 0
                && SATcollisionBox.Count <= 0)
            {
                Debug.WriteLine("[WARNING] " + this.GetType().ToString() + " : Entitiy without collision box trying to check collision.");
                return false;
            }

            //ignore disposed entity
            if (entity.Disposed)
                return false;

            //AABB
            for (int i = 0; i < entity.collisionBox.Count; i++)
            {
                if (!entity.collisionBox[i].Collidable)
                    continue;

                for (int j = 0; j < collisionBox.Count; j++)
                {
                    if (!collisionBox[j].Collidable)
                        continue;

                    if (entity.collisionBox[i].Collidable
                        && collisionBox[j].Collidable
                        && entity.collisionBox[i].Collide(collisionBox[j]))
                        return true;
                }
            }

            //SAT
            Vector2 MVT = Vector2.Zero;
            for (int i = 0; i < entity.SATcollisionBox.Count; i++)
            {
                if (!entity.SATcollisionBox[i].Collidable)
                    continue;

                for (int j = 0; j < SATcollisionBox.Count; j++)
                {
                    if (!SATcollisionBox[j].Collidable)
                        continue;

                    if (entity.SATcollisionBox[i].Collide(SATcollisionBox[j], out MVT))
                    {
                        //todo set MVT
                        return true;
                    }
                }
            }

            return false;
        }
        /// <summary>
        /// Check if this entity collide with another entity in entity array (SAT collision supported)
        /// </summary>
        /// <param name="entitiesToCheck">Entity list to check collision</param>
        /// <returns>Entity that collide (return null if not collide with any)</returns>
        public CoreEntity Collide(List<CoreEntity> entitiesToCheck = null)
        {
            if (entitiesToCheck == null)
                entitiesToCheck = GetSceneEntityList();

            //no collision box
            if (collisionBox.Count <= 0
                && SATcollisionBox.Count <= 0)
            {
                Debug.WriteLine("[WARNING] " + this.GetType().ToString() + " : Entitiy without collision box trying to check collision.");
                return null;
            }

            CoreEntity ent = null;

            for (int i = 0; i < entitiesToCheck.Count; i++)
            {
                //ignore same entity
                if (EntityID == entitiesToCheck[i].EntityID)
                    continue;
                //ignore disposed entity
                if (entitiesToCheck[i].Disposed)
                    continue;

                //AABB
                for (int j = 0; j < entitiesToCheck[i].collisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].collisionBox[j].Collidable)
                        continue;

                    for (int k = 0; k < collisionBox.Count; k++)
                    {
                        if (!collisionBox[k].Collidable)
                            continue;

                        if (entitiesToCheck[i].collisionBox[j].Collide(collisionBox[k]))
                            return entitiesToCheck[i];
                    }
                }

                //SAT
                Vector2 MVT = Vector2.Zero;
                for (int j = 0; j < entitiesToCheck[i].SATcollisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].SATcollisionBox[j].Collidable)
                        continue;

                    for (int k = 0; k < SATcollisionBox.Count; k++)
                    {
                        if (!SATcollisionBox[k].Collidable)
                            continue;

                        if (entitiesToCheck[i].SATcollisionBox[j].Collide(SATcollisionBox[k], out MVT))
                            return entitiesToCheck[i];
                    }
                }
            }

            return ent;
        }

        /// <summary>
        /// Check if given point is inside collision box of this entity
        /// </summary>
        /// <param name="point">Point position to check</param>
        /// <returns>Is point inside collision</returns>
        public bool Collide(Vector3 point)
        {
            //ignore disposed entity
            if (this.Disposed)
                return false;

            //no collision box
            if (collisionBox.Count <= 0
                && SATcollisionBox.Count <= 0)
            {
                Debug.WriteLine("[WARNING] " + this.GetType().ToString() + " : Entitiy without collision box trying to check collision.");
                return false;
            }

            //AABB
            for (int i = 0; i < collisionBox.Count; i++)
            {
                if (!collisionBox[i].Collidable)
                    continue;

                if (this.collisionBox[i].Collide(point))
                    return true;
            }

            //SAT
            Vector2 MVT = Vector2.Zero;
            for (int i = 0; i < SATcollisionBox.Count; i++)
            {
                if (!SATcollisionBox[i].Collidable)
                    continue;

                if (SATcollisionBox[i].Collide(point, out MVT))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Check if this entity collide with other entities in scene and return as entity list (SAT collision supported)
        /// </summary>
        /// <param name="entitiesToCheck">Entity array to check collision (optional)</param>
        /// <returns>Entity list that collide (return blank list if not collide with any)</returns>
        public List<CoreEntity> MultiCollide(List<CoreEntity> entitiesToCheck = null)
        {
            if (entitiesToCheck == null)
                entitiesToCheck = GetSceneEntityList();

            List<CoreEntity> res = new List<CoreEntity>();

            //return empty list if not collidable
            if (collisionBox.Count <= 0 && SATcollisionBox.Count <= 0)
            {
                Debug.WriteLine("[WARNING] " + this.GetType().ToString() + " : Entitiy without collision box trying to check collision.");
                return res;
            }

            for (int i = 0; i < entitiesToCheck.Count; i++)
            {
                //ignore disposed entity
                if (entitiesToCheck[i].Disposed)
                    continue;

                //skip same entity
                if (entitiesToCheck[i] == this)
                    continue;

                //skip existing entity
                if (res.FindIndex(e => e == entitiesToCheck[i]) != -1)
                    continue;

                bool alreadyCollide = false;

                for (int j = 0; j < entitiesToCheck[i].collisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].collisionBox[j].Collidable)
                        continue;
                    for (int k = 0; k < collisionBox.Count; k++)
                    {
                        if (!collisionBox[k].Collidable)
                            continue;

                        if (entitiesToCheck[i].collisionBox[j].Collide(collisionBox[k]))
                        {
                            res.Add(entitiesToCheck[i]);
                            alreadyCollide = true;
                        }
                    }
                }

                if (alreadyCollide)
                    continue;

                //SAT
                Vector2 MVT = Vector2.Zero;
                for (int j = 0; j < entitiesToCheck[i].SATcollisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].SATcollisionBox[j].Collidable)
                        continue;
                    for (int k = 0; k < SATcollisionBox.Count; k++)
                    {
                        if (!SATcollisionBox[k].Collidable)
                            continue;
                        if (entitiesToCheck[i].SATcollisionBox[j].Collide(SATcollisionBox[k], out MVT))
                        {
                            res.Add(entitiesToCheck[i]);
                        }
                    }
                }

            }

            return res;
        }

        /// <summary>
        /// Check if the point collide with entities in given entity list and return as entity list
        /// (SAT collision supported)
        /// </summary>
        /// <returns>Entity list that collide (return blank list if not collide with any)</returns>
        public static List<CoreEntity> MultiCollide(Vector3 point, List<CoreEntity> entitiesToCheck)
        {
            List<CoreEntity> res = new List<CoreEntity>();

            for (int i = 0; i < entitiesToCheck.Count; i++)
            {
                //ignore disposed entity
                if(entitiesToCheck[i].Disposed)
                    continue;

                //skip existing entity
                if (res.FindIndex(e => e == entitiesToCheck[i]) != -1)
                    continue;

                bool alreadyCollide = false;

                for (int j = 0; j < entitiesToCheck[i].collisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].collisionBox[j].Collidable)
                        continue;
                    if (entitiesToCheck[i].collisionBox[j].Collide(point))
                    {
                        res.Add(entitiesToCheck[i]);
                        alreadyCollide = true;
                    }
                }

                if (alreadyCollide)
                    continue;

                //SAT
                Vector2 MVT = Vector2.Zero;
                for (int j = 0; j < entitiesToCheck[i].SATcollisionBox.Count; j++)
                {
                    if (!entitiesToCheck[i].SATcollisionBox[j].Collidable)
                        continue;

                    if (entitiesToCheck[i].SATcollisionBox[j].Collide(point, out MVT))
                    {
                        res.Add(entitiesToCheck[i]);
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// Return position after calculated by z axis
        /// </summary>
        /// <returns></returns>
        public Vector2 GetDrawPosition()
        {
            if (activeScene.tiledMap != null)
                return new Vector2(Position.X, Position.Y - (Position.Z * activeScene.tiledMap.girdSize.Y));
            else
                return Vector2.Zero;
        }

        /// <summary>
        /// Set entity ID, this method should be called only by EntityManager. The uses of other conditions must be conscious.
        /// </summary>
        /// <param name="ID"></param>
        public void SetID(uint ID)
        {
            EntityID = ID;
        }

    }
}
