﻿//This class use for determine the target platform. Used to handle specific platform code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthwormEngine
{
    public static class PlatformManager
    {
        public static PlatformDevice ActivePlatform { get; private set; }

        public static void SetActivePlatform(PlatformDevice platform)
        {
            ActivePlatform = platform;
        }
    }

    public enum PlatformDevice
    {
        WINDOWS,
        DESKTOPGL,
        WINDOWSPHONE,
        ANDROID,
        IOS,
        MAC,
        TVOS,
        WATCHOS
    }
}
