﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace EarthwormEngine
{
    /// <summary>
    /// Array contain multiple entity for ease to manage
    /// </summary>
    public class EntityManager
    {
        /// <summary>
        /// List of entity
        /// </summary>
        public List<CoreEntity> entityList { get; set; }
        private IContentManager contentManager;
        private uint entityIDrun;

        /// <summary>
        /// Create EntityArray object 
        /// </summary>
        /// <param name="content">SceneManager ContentManager</param>
        public EntityManager(IContentManager content)
        {
            entityList = new List<CoreEntity>();
            entityIDrun = 0;
            contentManager = content;
        }

        /// <summary>
        /// Add new entity to array
        /// </summary>
        /// <param name="ent">Entity</param>
        /// <param name="pos">Entity position</param>
        /// <param name="angle">Sprite rotation angle</param>
        /// <param name="scene">Scene that contain this entity</param>
        public void AddEntity(CoreEntity ent, Vector3 pos, float angle, CoreScene scene)
        {
            ent.Position = pos;
            ent.Angle = angle;
            if (ent.EntityID == uint.MaxValue)
            {
                ent.SetID(entityIDrun);
                entityIDrun++;
            }
            ent.activeScene = scene;
            ent.Init();
            ent.Load(contentManager);
            entityList.Add(ent);
        }

        /// <summary>
        /// Add new entity to array
        /// </summary>
        /// <param name="entName">Entity name</param>
        /// <param name="ent">Entity</param>
        /// <param name="pos">Entity position</param>
        /// <param name="angle">Sprite rotation angle</param>
        /// <param name="scene">Scene that contain this entity</param>
        public void AddEntity(string entName, CoreEntity ent, Vector3 pos, float angle, CoreScene scene)
        {
            ent.EntityName = entName;
            ent.Position = pos;
            ent.Angle = angle;
            if (ent.EntityID == uint.MaxValue)
            {
                ent.SetID(entityIDrun);
                entityIDrun++;
            }
            ent.activeScene = scene;
            ent.Init();
            ent.Load(contentManager);
            entityList.Add(ent);
        }

        /// <summary>
        /// Remove entity from array
        /// </summary>
        /// <param name="ent">Entity in array</param>
        public void DeleteEntity(CoreEntity ent)
        {
            CoreEntity targetEnt = entityList.Find(e => e == ent);
            if (targetEnt != null)
            {
                entityList.Remove(targetEnt);
                targetEnt.DeleteEntity();
                targetEnt = null;
            }
        }

        /// <summary>
        /// Remove entity from array
        /// </summary>
        /// <param name="ent">index in array</param>
        public void DeleteEntity(int index)
        {
            if (index < entityList.Count)
            {
                entityList[index].DeleteEntity();
                entityList[index] = null;
                entityList.RemoveAt(index);
            }
        }

        /// <summary>
        /// Seek entity in array by its name
        /// </summary>
        /// <param name="entityName">Entity name</param>
        /// <returns></returns>
        public CoreEntity SeekEntity(string entityName)
        {
            CoreEntity ent = null;
            for (int i = 0; i < entityList.Count(); i++)
            {
                if (entityList[i].EntityName == entityName)
                {
                    ent = entityList[i];
                    break;
                }
            }
            return ent;
        }
        /// <summary>
        /// Seek entity in array by its ID
        /// </summary>
        /// <param name="EntityID">Entity ID</param>
        /// <returns></returns>
        public CoreEntity SeekEntity(uint EntityID)
        {
            CoreEntity ent = null;
            for (int i = 0; i < entityList.Count(); i++)
            {
                if (entityList[i].EntityID == EntityID)
                {
                    ent = entityList[i];
                    break;
                }
            }
            return ent;
        }

        /// <summary>
        /// Load all content of entities in array, should be called in scene load method
        /// </summary>
        public void Load()
        {
            for (int i = 0; i < entityList.Count; i++)
                entityList[i].Load(contentManager);
        }

        /// <summary>
        /// Update all entities in array, should be called in scene update
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < entityList.Count; i++)
            {
                //auto remove dispose entity
                if (entityList[i].Disposed)
                    DeleteEntity(entityList[i]);
                else if(entityList[i].Enable) //will not update disposed and disable entity
                    entityList[i].Update();

            }

        }

        /// <summary>
        /// Draw all sprites of entities in array, should be called in scene draw method
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        /// <param name="cameraViewPort">Viewport of current scene camera, use for optimize</param>
        public void Draw(SpriteBatch spriteBatch, Rectangle cameraViewPort, MapOrientation mapOrientation, float TileMapHeight)
        {
            for (int i = 0; i < entityList.Count; i++)
            {
                entityList[i].Draw(spriteBatch, cameraViewPort, mapOrientation, TileMapHeight);
            }
        }
    }
}
