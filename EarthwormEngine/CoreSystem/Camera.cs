﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormEngine
{
    /// <summary>
    /// This class handle game camera so you can see things.
    /// A scene should has only one object of this class.
    /// </summary>
    public class Camera
    {
        /// <summary>
        /// Get and set camera mode, the value is enum CameraMode.
        /// </summary>
        public CameraMode Mode { get; set; }
        /// <summary>
        /// Get and set camera pivot.
        /// Default is center of the screen
        /// </summary>
        public Vector2 Pivot { get; set; }
        /// <summary>
        /// Auto adjust scale to match with camera bound
        /// </summary>
        public bool AutoAdjustScale;

        /// <summary>
        /// Set more than 0 to prevent tile not render at edge of camera
        /// </summary>
        public uint CameraViewPortExtend = 20;

        private Vector2 ScaleLimit;
        private Rectangle ScreenLimit;
        private Vector2 Position { get { return currentPosition; } set { currentPosition = value; TargetPosition = value; StartPosition = value; } }
        private float Scale { get { return currentScale; } set { currentScale = value; TargetScale = value; StartScale = value; } }
        private float Angle { get { return currentAngle; } set { currentAngle = value; TargetAngle = value; StartAngle = value; } }

        private Vector2 currentPosition;
        private Vector2 StartPosition;
        private Vector2 TargetPosition;
        private float currentScale;
        private float StartScale;
        private float TargetScale;
        private float currentAngle;
        private float StartAngle;
        private float TargetAngle;
        //the speed solution use for calculate interpolation movement
        protected Func<float, float, float> interpolateFunction;
        private float interpolateTime = 0.0f;
        private float interpolateTimeScale = 1.0f;
        private float interpolateTimeForScale = 0.0f;
        private float interpolateTimeScaleForScale = 1.0f;
        private float interpolateTimeForRotate = 0.0f;
        private float interpolateTimeScaleForRotate = 1.0f;

        private float ShakeTime;
        private Vector2 ShakeRange;
        private Vector2 ShakeIntensity;

        /// <summary>
        /// Create camera object.
        /// </summary>
        public Camera()
        {
            Position = Vector2.Zero;
            Pivot = Calculator.GetScreenSize()/2;
            AutoAdjustScale = true;
            ScaleLimit = Vector2.Zero;
            ScreenLimit = Rectangle.Empty;
            Scale = 1.0f;
            Angle = 0.0f;
            Mode = CameraMode.Stable;

            TargetPosition = Position;
            TargetScale = Scale;
            TargetAngle = Angle;
            ShakeTime = 0.0f;
            ShakeRange = Vector2.Zero;
            ShakeIntensity = Vector2.Zero;
        }

        /// <summary>
        /// Initialize camera, this method should be in scene initialize.
        /// </summary>
        public void Init()
        {

        }

        /// <summary>
        /// Update camera system, this method should be in scene update.
        /// </summary>
        public void Update()
        {
            //move camera to new pos
            if (currentPosition != TargetPosition && interpolateTime < 1.0f)
            {
                interpolateTime += 1 / (interpolateTimeScale / (float)Calculator.GetElapsedGameTime());
                currentPosition = new Vector2(
                    Calculator.LinearInterpolation(StartPosition.X, TargetPosition.X, interpolateFunction(interpolateTime, 0.0f)),
                    Calculator.LinearInterpolation(StartPosition.Y, TargetPosition.Y, interpolateFunction(interpolateTime, 0.0f)));
            }
            else
            {
                interpolateTime = 0.0f;
                Position = TargetPosition;
            }

            //scale camera to new scale
            if (currentScale != TargetScale && interpolateTimeForScale < 1.0f)
            {
                interpolateTimeForScale += 1 / (interpolateTimeScaleForScale / (float)Calculator.GetElapsedGameTime());
                currentScale = 
                    Calculator.LinearInterpolation(StartScale, TargetScale, interpolateFunction(interpolateTimeForScale, 0.0f));
            }
            else
            {
                interpolateTimeForScale = 0.0f;
                Scale = TargetScale;
            }

            //rotate camera to new angle
            if (currentAngle != TargetAngle && interpolateTimeForRotate < 1.0f)
            {
                interpolateTimeForRotate += 1 / (interpolateTimeScaleForRotate / (float)Calculator.GetElapsedGameTime());
                currentAngle = Calculator.LinearInterpolation(StartAngle, TargetAngle, interpolateFunction(interpolateTimeForRotate, 0.0f));
            }
            else
            {
                interpolateTimeForRotate = 0.0f;
                Angle = TargetAngle;
            }

            //shake camera
            if (ShakeTime > 0.0f)
            {
                ShakeRange.X = Calculator.RandomFloat(ShakeIntensity.X * -0.5f, ShakeIntensity.X * 0.5f);
                ShakeRange.Y = Calculator.RandomFloat(ShakeIntensity.Y * -0.5f, ShakeIntensity.Y * 0.5f);
                ShakeTime -= Calculator.UnitsPerSecound(1.0f);
            }

            LimitCameraPosition();
            LimitCameraScale();

            //scale camera to bounding box
            if (AutoAdjustScale && ScaleLimit != Vector2.Zero && !ScreenLimit.IsEmpty)
            {
                //width
                if (Calculator.GetScreenSize().X / Scale > ScreenLimit.Width)
                {
                    float newScale = (float)Calculator.GetScreenSize().X / (float)ScreenLimit.Width;
                    if (ScaleLimit.Y < newScale) ScaleLimit.Y = newScale;
                    Scale = newScale;
                    TargetScale = newScale;
                }
                //height
                if (Calculator.GetScreenSize().Y / Scale > ScreenLimit.Height)
                {
                    float newScale = (float)Calculator.GetScreenSize().Y / (float)ScreenLimit.Height;
                    if (ScaleLimit.Y < newScale) ScaleLimit.Y = newScale;
                    Scale = newScale;
                    TargetScale = newScale;
                }
            }
        }

        /// <summary>
        /// Return matrix for SpriteBatch.Begin()
        /// </summary>
        public Matrix GetCameraTranformation()
        {
            Vector2 pos = Position + (new Vector2(ResolutionManager.getMarginSize().Y/2, ResolutionManager.getMarginSize().X/2)*ResolutionManager.getVirtualAspectRatio()) + ResolutionManager.getMarginSize();
            //Debug.WriteLine(ResolutionManager.getVirtualAspectRatio());
            return Matrix.CreateTranslation(new Vector3((pos.X + ShakeRange.X)*-1, (pos.Y + ShakeRange.Y)*-1, 0))
                * Matrix.CreateRotationZ(Calculator.DegreeToRadians(Angle))
                * Matrix.CreateScale(Scale * ResolutionManager.getScale().X, Scale * ResolutionManager.getScale().Y, 1)
                * Matrix.CreateTranslation(new Vector3(Pivot.X, Pivot.Y, 0));
        }

        /// <summary>
        /// Set camera position (no smooth movement (slow down)).
        /// </summary>
        /// <param name="position">New position</param>
        public void SetPosition(Vector2 position)
        {
            Position = position;
            TargetPosition = position;
            LimitCameraPosition();
        }
        /// <summary>
        /// Get camera position.
        /// </summary>
        /// <returns>Vector 2D Position</returns>
        public Vector2 GetPosition()
        {
            return TargetPosition;
        }
        /// <summary>
        /// Add vector 2D direction and speed to camera position (no smooth movement (slow down)).
        /// </summary>
        /// <param name="position">Vector 2D direction and size</param>
        public void AddToPosition(Vector2 position)
        {
            Position += position;
            TargetPosition = Position;
            LimitCameraPosition();
        }
        /// <summary>
        /// Set destination for camera panning (with smooth movement (slow down)).
        /// </summary>
        /// <param name="position">Destination Position</param>
        /// <param name="time">Time to move (second)</param>
        /// <returns>Is reaching new position</returns>
        public bool MoveCameraTo(Vector2 position, float time)
        {
            TargetPosition = position;
            interpolateTimeScale = time;
            StartPosition = Position;
            interpolateTime = 0;
            interpolateFunction = DefaultInterpolateFunction;

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }
        /// <summary>
        /// Set destination for camera panning (with smooth movement (slow down)).
        /// </summary>
        /// <param name="position">Destination Position</param>
        /// <param name="time">Time to move (second)</param>
        /// <param name="solution">Custom solution for liner movement speed</param>
        /// <returns>Is reaching new position</returns>
        public bool MoveCameraTo(Vector2 position, float time, Func<float, float,float> solution)
        {
            TargetPosition = position;
            interpolateTimeScale = time;
            StartPosition = Position;
            interpolateTime = 0;
            interpolateFunction = solution;

            if (currentPosition != position && interpolateTime < 1) return false;
            else return true;
        }
        /// <summary>
        /// Set scale for camera zooming (no smooth movement (slow down)).
        /// </summary>
        /// <param name="scale">Scale value (positive value for zoom in, negative value for zoom out)</param>
        public void SetScale(float scale)
        {
            TargetScale = scale;
            Scale = scale;
            LimitCameraScale();
        }

        /// <summary>
        /// Get current camera scale value.
        /// </summary>
        /// <returns>Scale</returns>
        public float GetScale()
        {
            return TargetScale;
        }
        /// <summary>
        /// Add scale value to camera zoomming (no smooth movement (slow down)).
        /// </summary>
        /// <param name="scale">Scale value (positive value for zoom in, negative value for zoom out)</param>
        public void AddToScale(float scale)
        {
            Scale += scale;
            LimitCameraScale();
        }
        /// <summary>
        /// Set destination scale value for camera zoomming (with smooth movement (slow down)).
        /// </summary>
        /// <param name="scale">Scale value</param>
        /// <param name="time">Time to scale (Second)</param>
        /// <returns>Is reaching new scale</returns>
        public bool ScaleCameraTo(float scale, float time)
        {
            TargetScale = scale;
            interpolateTimeScaleForScale = time;
            StartScale = currentScale;
            interpolateTimeForScale = 0;
            interpolateFunction = DefaultInterpolateFunction;

            if (currentScale != scale && interpolateTimeForScale < 1) return false;
            else return true;
        }

        /// <summary>
        /// Set destination scale value for camera zoomming (with smooth movement (slow down)).
        /// </summary>
        /// <param name="scale">Scale value</param>
        /// <param name="time">Time to scale (Second)</param>
        /// <param name="solution">Custom solution for liner movement speed</param>
        /// <returns>Is reaching new scale</returns>
        public bool ScaleCameraTo(float scale, float time, Func<float, float, float> solution)
        {
            TargetScale = scale;
            interpolateTimeScaleForScale = time;
            StartScale = currentScale;
            interpolateTimeForScale = 0;
            interpolateFunction = solution;

            if (currentScale != scale && interpolateTimeForScale < 1) return false;
            else return true;
        }

        /// <summary>
        /// Get current camera rotation angle.
        /// </summary>
        /// <returns>Angle</returns>
        public float GetAngle()
        {
            return TargetAngle;
        }
        /// <summary>
        /// Set camera rotation angle (no smooth movement (slow down)).
        /// eg: SetAngle(360)
        /// </summary>
        /// <param name="angle">Angle value (Degree)</param>
        public void SetAngle(float angle)
        {
            TargetAngle = angle;
            Angle = angle;
        }
        /// <summary>
        /// Add angle value to current camera rotation angle (no smooth movement (slow down)).
        /// eg: AddToAngle(360)
        /// </summary>
        /// <param name="angle">Angle value (Degree)</param>
        public void AddToAngle(float angle)
        {
            TargetAngle = angle;
            Angle = angle;
        }
        /// <summary>
        /// Set destination angle for camera rotation (with smooth movement (slow down)).
        /// eg: RotateAngleTo(360)
        /// </summary>
        /// <param name="angle">Angle value (Degree)</param>
        /// <param name="time">Time to rotate (Second)</param>
        /// <returns>Is reaching new rotation</returns>
        public bool RotateAngleTo(float angle, float time)
        {
            TargetAngle = angle;
            interpolateTimeScaleForRotate = time;
            StartAngle = Angle;
            interpolateTimeForRotate = 0;
            interpolateFunction = DefaultInterpolateFunction;

            if (currentAngle != angle && interpolateTimeForRotate < 1) return false;
            else return true;
        }
        /// <summary>
        /// Set destination angle for camera rotation (with smooth movement (slow down)).
        /// eg: RotateAngleTo(360)
        /// </summary>
        /// <param name="angle">Angle value (Degree)</param>
        /// <param name="time">Time to rotate (Second)</param>
        /// <param name="solution">Custom solution for liner movement speed</param>
        /// <returns>Is reaching new rotation</returns>
        public bool RotateAngleTo(float angle, float time, Func<float, float, float> solution)
        {
            TargetAngle = angle;
            interpolateTimeScaleForRotate = time;
            StartAngle = Angle;
            interpolateTimeForRotate = 0;
            interpolateFunction = solution;

            if (currentAngle != angle && interpolateTimeForRotate < 1) return false;
            else return true;
        }

        /// <summary>
        /// Set screen limit using game world coordinate, camera will not move outside of this rectagle box,
        /// Must be larger of same as screen size,
        /// default value is empty,
        /// Set as empty to disable limiter
        /// </summary>
        /// <param name="space">Rectangle space</param>
        public void SetCameraBound(Rectangle space)
        {
            ScreenLimit = space;
            if (!AutoAdjustScale)
            {
                if (ScreenLimit.Width < Calculator.GetScreenSize().X) ScreenLimit.Width = (int)Calculator.GetScreenSize().X;
                if (ScreenLimit.Height < Calculator.GetScreenSize().Y) ScreenLimit.Height = (int)Calculator.GetScreenSize().Y;
            }
        }

        /// <summary>
        /// Get camera bound, Camera will not move outside of this rectagle box
        /// </summary>
        /// <returns>Rectangle bounding box</returns>
        public Rectangle GetCameraBound()
        {
            return ScreenLimit;
        }

        /// <summary>
        /// Set camera scale limit, camera will not scale more or less than this limit,
        /// minimum value must be less than 1.0f and maximum value must be more that 1.0f,
        /// default value is (0,0),
        /// set as zero vector to disable scale limit
        /// </summary>
        /// <param name="limit">Vector 2D (minimum,maximum)</param>
        public void SetCameraScaleLimit(Vector2 limit)
        {
            ScaleLimit = limit;
        }

        /// <summary>
        /// Get camera scale limit, camera will not scale more or less than this limit
        /// </summary>
        /// <returns>Vector 2D (minimum,maximum)</returns>
        public Vector2 GetCameraScaleLimit()
        {
            return ScaleLimit;
        }

        /// <summary>
        /// Shake camera, set time to 0.0f to stop shaking
        /// </summary>
        /// <param name="Intensity">Shake intensity by coordinate</param>
        /// <param name="Time">Shake time (secounds)</param>
        public void ShakeCamera(Vector2 Intensity, float Time)
        {
            ShakeTime = Time;
            ShakeIntensity = Intensity;
        }

        /// <summary>
        /// Get the camera viewport relate to game world coordinate
        /// </summary>
        /// <returns>Rectangle viewport in game world coordinate</returns>
        public Rectangle GetCameraViewPort()
        {
            Vector2 pos = Position + (new Vector2(ResolutionManager.getMarginSize().Y / 2, ResolutionManager.getMarginSize().X / 2) * ResolutionManager.getVirtualAspectRatio()) + ResolutionManager.getMarginSize();
            float scale = Scale * ResolutionManager.getScale().X;
            return new Rectangle((int)((pos.X - Pivot.X / scale) - CameraViewPortExtend),
                (int)((pos.Y - Pivot.Y / scale) - CameraViewPortExtend),
                (int)((pos.X + (Calculator.GetScreenSize().X - Pivot.X) / scale) + CameraViewPortExtend),
                (int)((pos.Y + (Calculator.GetScreenSize().Y - Pivot.Y) / scale) + CameraViewPortExtend));
        }

        /// <summary>
        /// Gat world position after calculate by camera (use for draw in world related position UI, etc.)
        /// </summary>
        /// <param name="position">Wrold relative position</param>
        /// <returns></returns>
        public Vector2 GetWorldRelativePosition(Vector2 position)
        {
            Matrix invert = Matrix.Invert(GetCameraTranformation());
            return Vector2.Transform(position, invert);
        }

        //limit camera position
        private void LimitCameraPosition()
        {
            if (!ScreenLimit.IsEmpty)
            {
                float scale = Scale * ResolutionManager.getScale().X;
                if (Position.X - (Pivot.X / scale) < ScreenLimit.X) currentPosition.X = ScreenLimit.X + (Pivot.X / scale);
                else if (Position.X + ((Calculator.GetScreenSize().X - Pivot.X) / scale) > ScreenLimit.Width) currentPosition.X = ScreenLimit.Width - ((Calculator.GetScreenSize().X - Pivot.X) / scale);
                if (Position.Y - (Pivot.Y / scale) < ScreenLimit.Y) currentPosition.Y = ScreenLimit.Y + (Pivot.Y / scale);
                else if (Position.Y + ((Calculator.GetScreenSize().Y - Pivot.Y) / scale) > ScreenLimit.Height) currentPosition.Y = ScreenLimit.Height - ((Calculator.GetScreenSize().Y - Pivot.Y) / scale);

                if (TargetPosition.X - (Pivot.X / scale) < ScreenLimit.X) TargetPosition.X = ScreenLimit.X + (Pivot.X / scale);
                else if (TargetPosition.X + ((Calculator.GetScreenSize().X - Pivot.X) / scale) > ScreenLimit.Width) TargetPosition.X = ScreenLimit.Width - ((Calculator.GetScreenSize().X - Pivot.X) / scale);
                if (TargetPosition.Y - (Pivot.Y / scale) < ScreenLimit.Y) TargetPosition.Y = ScreenLimit.Y + (Pivot.Y / scale);
                else if (TargetPosition.Y + ((Calculator.GetScreenSize().Y - Pivot.Y) / scale) > ScreenLimit.Height) TargetPosition.Y = ScreenLimit.Height - ((Calculator.GetScreenSize().Y - Pivot.Y) / scale);
            } 
        }

        //limit camera scale
        private void LimitCameraScale()
        {
            if (ScaleLimit != Vector2.Zero)
            {
                if (Scale < ScaleLimit.X) Scale = ScaleLimit.X;
                else if (Scale > ScaleLimit.Y) Scale = ScaleLimit.Y;

                if (TargetScale < ScaleLimit.X) TargetScale = ScaleLimit.X;
                else if (TargetScale > ScaleLimit.Y) TargetScale = ScaleLimit.Y;
            }
        }

        //default solution for linear interpolation movement
        protected float DefaultInterpolateFunction(float x, float n)
        {
            //while x is between 0 to 1

            //ease in ease out
            return (float)Math.Pow(x, 2) * (3.0f - 2.0f * x);

            //ease in
            //return (float)Math.Pow(x, 2);

            //ease out
            //return x*(2-x);
        }
    }
}
