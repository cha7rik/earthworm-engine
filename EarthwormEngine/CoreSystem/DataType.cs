﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthwormEngine
{
    /// <summary>
    /// Direction
    /// </summary>
    public enum FaceDirection
    {
        Down,
        Left,
        Up,
        Right,
        None

    }
    /// <summary>
    /// Camera mode
    /// </summary>
    public enum CameraMode
    {
        Stable
    }
}
