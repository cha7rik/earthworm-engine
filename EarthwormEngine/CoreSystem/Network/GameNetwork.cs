﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace EarthwormEngine.Network
{
    public static class GameNetwork
    {
        public static NetServer Server;
        public static NetClient Client;
        private static string hostIP;
        private static int port;
        public static NetPeerConfiguration Config;
        public static NetIncomingMessage IncomingMessage;
        public static NetOutgoingMessage OutgoingMessage;
        public static List<NetIncomingMessage> IncomingMessageList;

        public static void Init(string AppIdentifier, int Port, int MaximumConnections, bool isServer)
        {
            if (isServer)
            {
                port = Port;
                Config = new NetPeerConfiguration(AppIdentifier);
                Config.Port = port;
                Config.MaximumConnections = MaximumConnections;
                Config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
                Config.ConnectionTimeout = 10.0f;
                Server = new NetServer(Config);
            }
            else
            {
                Config = new NetPeerConfiguration(AppIdentifier);
                Config.ConnectionTimeout = 10.0f;
                Client = new NetClient(Config);
            }
        }

        public static bool ConnectToServer(string IP, int Port, PacketType Type, string Message)
        {
            hostIP = IP;
            port = Port;
            OutgoingMessage = Client.CreateMessage();
            OutgoingMessage.Write((byte)Type);
            OutgoingMessage.Write(Message);
            try
            {
                Client.Connect(hostIP, port, OutgoingMessage);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ApproveConnection(string Message)
        {
            bool result = false;
            GameNetwork.IncomingMessageList = new List<NetIncomingMessage>();
            GameNetwork.Server.ReadMessages(GameNetwork.IncomingMessageList);
            if (GameNetwork.IncomingMessageList != null && GameNetwork.IncomingMessageList.Count > 0)
            {
                List<NetConnection> senders = new List<NetConnection>();
                //check if login message
                foreach (NetIncomingMessage incMsg in GameNetwork.IncomingMessageList)
                {
                    if (incMsg.MessageType == NetIncomingMessageType.ConnectionApproval &&
                        incMsg.ReadByte() == (byte)PacketType.LOGIN)
                    {
                        //approve login
                        if (!string.IsNullOrEmpty(Message))
                        {
                            GameNetwork.OutgoingMessage = GameNetwork.Server.CreateMessage();
                            GameNetwork.OutgoingMessage.Write(Message);
                            incMsg.SenderConnection.Approve(GameNetwork.OutgoingMessage);
                        }
                        else
                            incMsg.SenderConnection.Approve();

                        result = true;
                    }
                }

                Server.Recycle(IncomingMessageList);
            }

            return result;
        }
    }
}
