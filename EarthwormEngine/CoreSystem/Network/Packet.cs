﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormEngine.Network
{
    public class Packet
    {
        public string EntityName { get; set; }
        public uint EntityID { get; set; }
        public float PositionX { get; set; }
        public float PositionY { get; set; }
        public byte FaceDir { get; set; }
        public float DirX { get; set; }
        public float DirY { get; set; }
    }

    public enum PacketType
    {
        LOGIN,
        APPROVE,
        ENTITYINFO,
        SCENESTATE,
        READYCHECK,
        GAMEEND
    }
}
