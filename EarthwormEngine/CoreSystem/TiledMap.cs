﻿///*
///Earthworm Engine Tiled Map
///Some function form tile map software "Tiled" version 1.3.4 is supported
///-Map Orientation : orthogonal, isometric
///-Json map reader
///-Collision field (Count every objects in object layer named "collision")
///-Multi-layer
///-Animated tiled (Separate animate duration is not supported, only use last frame's duration apply whole animation)
///-Add layer custom property named "depth" to specified depth of layer. (default is LayerDepth)
///-Custom Entity (Count every objects in object layer named "custom") can read custom properties in "Type" as string
///-Tile size on the map must be the same as tile size on texture
///
/// *IMPORTENT* The tile map data must be exported with tilesets embeded (Edit > Preference > Export Option > Embed tilesets)
/// 
///============ISOMETRIC TILED SYSTEM===============
///This is how isometric layer order works:
///0.[000]1000 = y position count by grid, support 0-999 (position y / map tile size y) + 0.00010 to prevent bug if entity position is out of bound : /1000
///0.000[10]00 = y position on current tile, support 0-99 (this will set to half of map tile height for vertical tile) : /100000
///0.00010[00] = custom depth is the last priority, support 0-99 (more than 99 will cause glitch since float can only hold 7 digits) /100000
///
/// Add custom variable for each layer named 'z' as int, this will determine the z high for certain layer
/// Vertical tile option : Add bool custom properties in Tiled map editor named "vertical" with value "true"
/// This wil set pivot at half of tile height so any vertical entity will not overlapse the texture
/// *Vertical Layer only works on isometric map
///=================================================
///*


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using EarthwormEngine.CollisionSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// Depth calculation for isometric map (unavailable)
    /// </summary>
    /// <param name="defaultDepth">if map is orthogonal, will retrun this value</param>
    /// <returns>calculated depth</returns>
    delegate float IsomecticDepthCalculation(float defaultDepth);

    /// <summary>
    /// TileMap system for scene
    /// </summary>
    public class TileMap
    {
        protected List<TileMapLayer> tileMapLayer;
        protected Texture2D tileMapTexture;
        /// <summary>
        /// Map size (count as tiles)
        /// </summary>
        public Vector2 mapSize { get; protected set; }
        /// <summary>
        /// Tile size in map (count as pixel per tile)
        /// </summary>
        public Vector2 girdSize { get; protected set; }
        /// <summary>
        /// Tile size in sprite sheet (count as pixel per tile)
        /// </summary>
        public Vector2 spriteTileSize { get; protected set; }

        private List<Vector2> tilesetNumber;
        private Rectangle tileDraw;
        private List<AnimateTile> animateTiles;

        //private List<float> layersDepth;
        /// <summary>
        /// Tilemap layer depth (Default)
        /// </summary>
        public float LayerDepth = 0.4f;

        protected CoreScene currentScene;
        /// <summary>
        /// Create new tilemap (orthogonal)
        /// </summary>
        /// <param name="data">Tilemap data (list of tile number eg:1,0,0,0,1)</param>
        /// <param name="MapSize">Tilemap size</param>
        /// <param name="TiledSize">Tile size</param>
        /// <param name="TextureName">Tilemap texture name</param>
        /// <param name="scene">Tilemap scene</param>
        public TileMap(List<string> data, Vector2 MapSize, Vector2 TiledSize, string TextureName, CoreScene scene) 
        {
            if (TextureName.Length > 0)
            {
                tileMapTexture = scene.sceneManager.GetContentManager().Load<Texture2D>(TextureName);
                //create tileset number
                tilesetNumber = new List<Vector2>();
                for (int i = 0; i < tileMapTexture.Width / TiledSize.X; i++)
                    for (int j = 0; j < tileMapTexture.Height / TiledSize.Y; j++)
                        tilesetNumber.Add(new Vector2(i*TiledSize.X,j*TiledSize.Y));
            }
            mapSize = MapSize;
            girdSize = TiledSize;

            tileMapLayer = new List<TileMapLayer>();

            //format for data is like "0,0,0,0,1,0,1"
            for (int i = 0; i < data.Count; i++) //loop for each layer
            {
                TileMapLayer layer = new TileMapLayer();
                List<byte> layerData = new List<byte>();

                string[] d = data[i].Split(',');
                for (int j = 0; j < d.Length; j++)
                    layerData.Add(byte.Parse(d[j]));

                layer.data = layerData;
                tileMapLayer.Add(layer);
            }

            currentScene = scene;
            tileDraw = new Rectangle();
        }

        /// <summary>
        /// *Must be called after scene initialize*
        /// Create tilemap by import form json (exported form Tiled Map Editor)
        /// Create collision by add an object layer named "collision", any object in this layer will be consider as collision box.
        /// Add layer custom property named "depth" to specified depth of layer. (default is LayerDepth)
        /// </summary>
        /// <param name="jsonData">Data as json format</param>
        /// <param name="textureName">Texture path and name</param>
        /// <param name="contentManager">SceneManager ContentManager</param>
        /// <param name="scene">Tilemap scene</param>
        public TileMap(string jsonData, string textureName, IContentManager contentManager, CoreScene scene)
        {
            currentScene = scene;
            tileDraw = new Rectangle();

            JObject jsonMap = JObject.Parse(jsonData);

            //check orientation
            if ((string)jsonMap["orientation"] == "orthogonal" && currentScene.MapOrientation != MapOrientation.Orthogonal)
                throw new Exception("[Error] TiledMap : Map orientation is not match");
            if ((string)jsonMap["orientation"] == "isometric" && currentScene.MapOrientation != MapOrientation.Isometric)
                throw new Exception("[Error] TiledMap : Map orientation is not match");

            //get map property from json object
            mapSize = new Vector2((int)jsonMap["width"], (int)jsonMap["height"]);
            girdSize = new Vector2((int)jsonMap["tilewidth"], (int)jsonMap["tileheight"]);
            //check if there are tileset data
            if (jsonMap["tilesets"].Count() > 0)
                spriteTileSize = new Vector2((int)jsonMap["tilesets"].First["tilewidth"], (int)jsonMap["tilesets"].First["tileheight"]);
            else
                Debug.WriteLine("[WARNING] TiledMap : There is no tile set data in json map data.");

            if (!string.IsNullOrEmpty(textureName))
            {
                tileMapTexture = contentManager.Load<Texture2D>(textureName);
                //create tileset number
                tilesetNumber = new List<Vector2>();
                for (int i = 0; i < tileMapTexture.Height / girdSize.Y; i++)
                    for (int j = 0; j < tileMapTexture.Width / girdSize.X; j++)
                        tilesetNumber.Add(new Vector2(j * spriteTileSize.X, i * spriteTileSize.Y));
            }

            // get JSON map property objects into a list
            IList<JToken> layers = jsonMap["layers"].Children().ToList();
            tileMapLayer = new List<TileMapLayer>();
            foreach (JToken layer in layers)
            {
                TileMapLayer l = JsonConvert.DeserializeObject<TileMapLayer>(layer.ToString());
                float depth = LayerDepth;
                int z = 0;
                if (layer["properties"] != null)
                {
                    JToken prop = layer["properties"];
                    for(int i = 0;i < prop.Count();i++)
                    {
                        //get custom depth from map
                        if (prop[i]["name"] != null && (string)prop[i]["name"] == "depth")
                            depth = (float)prop[i]["value"];
                        //check z layer for isometric map
                        if (prop[i]["name"] != null && (string)prop[i]["name"] == "z")
                            z = (int)prop[i]["value"];
                        //check if vertical layer
                        if (prop[i]["name"] != null && (string)prop[i]["name"] == "vertical")
                            l.IsVerticalLayer = (bool)prop[i]["value"];
                    }
                    //set z value
                    l.Z = z;
                    l.depth = depth;
                }
                //store layer properties
                if (currentScene.MapOrientation == MapOrientation.Isometric) //if it isometric map, lower priority of custom depth
                {
                    //custom depth
                    //custom depth disable on isometric map
                    //depth = depth / 10000 + LayerDepth;
                    //z axis
                    depth += (z / 1.0f);
                    //+0.01 is for prevent minus position of y axis that cause z axis bug
                    depth += 0.1f;
                }
                //set tilePivot
                l.tilePivot = spriteTileSize - girdSize;

                //find layer order
                int tileInSameZ = 0;
                for (int i = 0; i < tileMapLayer.Count; i++)
                    if (tileMapLayer[i].Z == l.Z)
                        tileInSameZ++;
                l.LayerOrder = tileInSameZ;

                tileMapLayer.Add(l);
            }
            //convert data
            for (int i = 0; i < tileMapLayer.Count; i++)
                if (tileMapLayer[i].type.Equals("tilelayer"))
                {
                }
                //add collision entity
                else if (tileMapLayer[i].type.Equals("objectgroup"))
                {
                    if (tileMapLayer[i].name.Equals("collision"))
                        for (int j = 0; j < tileMapLayer[i].objects.Count; j++)
                        {
                            AddCollisionArea(new Vector3(tileMapLayer[i].objects[j].x, tileMapLayer[i].objects[j].y, tileMapLayer[i].Z),
                                new Vector2(tileMapLayer[i].objects[j].width, tileMapLayer[i].objects[j].height));
                        }
                    else if (tileMapLayer[i].name.Equals("custom"))
                        for (int j = 0; j < tileMapLayer[i].objects.Count; j++)
                        {
                            AddCustomEntity(tileMapLayer[i].objects[j].name,
                                new Vector3(tileMapLayer[i].objects[j].x, tileMapLayer[i].objects[j].y, tileMapLayer[i].Z),
                                new Vector2(tileMapLayer[i].objects[j].width, tileMapLayer[i].objects[j].height),
                                tileMapLayer[i].objects[j].type);
                        }
                }

            //tiled animation
            //check if there are any animation in Json tiled map
            if (jsonMap["tilesets"] != null && jsonMap["tilesets"].Count() > 0)
            {
                animateTiles = new List<AnimateTile>();

                if (jsonMap["tilesets"].First["tiles"] != null)
                {
                    IList<JToken> animateTileJToken = jsonMap["tilesets"].First["tiles"].Children().ToList();

                    if (animateTileJToken.Count > 0)
                    {
                        for (int i = 0; i < animateTileJToken.Count; i++)
                        {
                            AnimateTile animateTile = new AnimateTile(new Rectangle(0, 0, tileMapTexture.Width, tileMapTexture.Height), spriteTileSize);

                            JToken jprop = animateTileJToken[i];
                            animateTile.tileID = int.Parse(jprop["id"].ToString());

                            IList<JToken> a = jprop["animation"].Children().ToList();
                            if (a != null)
                                for (int j = 0; j < a.Count; j++)
                                {
                                    animateTile.animationFrames.Add(int.Parse(a[j]["tileid"].ToString()));
                                    //convert millisecound to frame
                                    animateTile.speed = (uint)Math.Ceiling((float)a[j]["duration"] * 0.06f);
                                }

                            animateTiles.Add(animateTile);
                        }

                    }
                }
            }

        }

        /*
        /// <summary>
        /// Create tilemap by import form json file(export form Tiled Map Editor)
        /// </summary>
        /// <param name="path">json data file path</param>
        /// <param name="contentManager">SceneManager ContentManager</param>
        /// <param name="scene">Tilemap scene</param>
        public TileMap(string path, ContentManager contentManager, Scene scene)
        {
            currentScene = scene;
            tileDraw = new Rectangle();

            //get json map as string
            string stringMap = "";
            using (StreamReader reader = new StreamReader(File.OpenRead(path)))
            {
                stringMap = reader.ReadToEnd();
            }
            JObject jsonMap = JObject.Parse(stringMap);

            //get map property from json object
            mapSize = new Vector2((int)jsonMap["width"], (int)jsonMap["height"]);
            tileSize = new Vector2((int)jsonMap["tilewidth"], (int)jsonMap["tileheight"]);
            if (((string)jsonMap["tilesets"].First["name"]).Length > 0)
            {
                tileMapTexture = contentManager.Load<Texture2D>("resources/tilesets/" + (string)jsonMap["tilesets"].First["name"]);
                //create tileset number
                tilesetNumber = new List<Vector2>();
                for (int i = 0; i < tileMapTexture.Height / tileSize.Y; i++)
                    for (int j = 0; j < tileMapTexture.Width / tileSize.X; j++)
                        tilesetNumber.Add(new Vector2(j * tileSize.X, i * tileSize.Y));
            }

            // get JSON map property objects into a list
            IList<JToken> layers = jsonMap["layers"].Children().ToList();
            List<TileMapLayer> tiledMapLayers = new List<TileMapLayer>();
            foreach (JToken layer in layers)
            {
                TileMapLayer l = JsonConvert.DeserializeObject<TileMapLayer>(layer.ToString());
                tiledMapLayers.Add(l);
            }
            //convert data
            tileMapData = new List<List<byte>>();
            for (int i = 0; i < tiledMapLayers.Count; i++)
                if (tiledMapLayers[i].type.Equals("tilelayer"))
                    tileMapData.Add(tiledMapLayers[i].data);
            //add collision entity
            for (int i = 0; i < tiledMapLayers.Count; i++)
                if (tiledMapLayers[i].type.Equals("objectgroup")
                    && tiledMapLayers[i].name.Equals("collision"))
                    for (int j = 0; j < tiledMapLayers[i].objects.Count; j++)
                    {
                        AddCollisionArea(new Vector2(tiledMapLayers[i].objects[j].x,tiledMapLayers[i].objects[j].y),
                            new Vector2(tiledMapLayers[i].objects[j].width, tiledMapLayers[i].objects[j].height));
                    }
        }
        */

        public void Draw(SpriteBatch spriteBatch, Rectangle cameraViewPort)
        {
            //setup draw for orthogonal
            if (currentScene.MapOrientation == MapOrientation.Orthogonal)
            {
                for (int i = 0; i < tileMapLayer.Count; i++)
                {
                    int row = -1;
                    if (tileMapLayer[i].data != null)
                        for (int j = 0; j < tileMapLayer[i].data.Count; j++)
                        {
                            //check if empty tile
                            if (tileMapLayer[i].data[j] > 0)
                            {
                                if (j % mapSize.X == 0)
                                    row++;
                                Vector2 drawPos = new Vector2((j % mapSize.X) * girdSize.X, row * girdSize.Y) - tileMapLayer[i].tilePivot;

                                //check if animate tile
                                if (animateTiles != null && animateTiles.Count > 0)
                                {
                                    AnimateTile animatetile = animateTiles.Where(item => item.tileID == (int)tileMapLayer[i].data[j] - 1).FirstOrDefault();
                                    if (animatetile != null)
                                    {
                                        //update animation
                                        animatetile.animation.Animate(animatetile.animationFrames, animatetile.speed);
                                        //draw only in camera viewport
                                        if (drawPos.X + girdSize.X >= cameraViewPort.X
                                            && drawPos.Y + girdSize.Y >= cameraViewPort.Y
                                            && drawPos.X <= cameraViewPort.Width
                                            && drawPos.Y <= cameraViewPort.Height)
                                            spriteBatch.Draw(tileMapTexture, drawPos, animatetile.animation.GetSpriteAnimation(), Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, (float)tileMapLayer[i].depth + ((float)i / 100));

                                        continue;
                                    }
                                }
                                tileDraw.X = (int)tilesetNumber[tileMapLayer[i].data[j] - 1].X;
                                tileDraw.Y = (int)tilesetNumber[tileMapLayer[i].data[j] - 1].Y;
                                tileDraw.Width = (int)spriteTileSize.X;
                                tileDraw.Height = (int)spriteTileSize.Y;

                                //draw only in camera viewport
                                if (drawPos.X + spriteTileSize.X >= cameraViewPort.X
                                    && drawPos.Y + spriteTileSize.Y >= cameraViewPort.Y
                                    && drawPos.X <= cameraViewPort.Width
                                    && drawPos.Y <= cameraViewPort.Height)
                                    spriteBatch.Draw(tileMapTexture, drawPos, tileDraw, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, (float)tileMapLayer[i].depth + ((float)i / 100));
                            }
                            else
                                if (j % mapSize.X == 0)
                                row++;
                        }
                }
            }
            //setup draw for isometric
            else if (currentScene.MapOrientation == MapOrientation.Isometric)
            {
                float depth = 0;
                //todo calculate layer depth on y axis
                for (int i = 0; i < tileMapLayer.Count; i++)
                {
                    int row = -1;

                    float verticalDepth = 0;
                    //if layer is vertical layer, precalculate position on tile as half of map tile height
                    if (tileMapLayer[i].IsVerticalLayer)
                        verticalDepth = (girdSize.Y / 2) / 100000f;

                    if (tileMapLayer[i].data != null)
                    for (int j = 0; j < tileMapLayer[i].data.Count; j++)
                    {
                            //check if empty tile
                            if (tileMapLayer[i].data[j] > 0)
                            {
                                if (j % mapSize.X == 0)
                                    row++;
                                //for isometric map, will always draw tile texture from bottom
                                Vector2 drawPos = new Vector2((j % mapSize.X) * (girdSize.X / 2) - (row * ((girdSize.X / 2))), (row * (girdSize.Y / 2)) + ((j % mapSize.X) * (girdSize.Y / 2))) - new Vector2(girdSize.X / 2, 0);

                                //save actual y position of tile to calculate depth
                                float drawposY = drawPos.Y;

                                drawPos -= tileMapLayer[i].tilePivot;
                                //draw y position according to z position
                                drawPos -= new Vector2(0, tileMapLayer[i].Z * girdSize.Y);

                                //draw only in camera viewport
                                if (drawPos.X + spriteTileSize.X >= cameraViewPort.X
                                    && drawPos.Y + spriteTileSize.Y >= cameraViewPort.Y
                                    && drawPos.X <= cameraViewPort.Width
                                    && drawPos.Y <= cameraViewPort.Height)
                                {
                                    //calculate position on spritesheet to draw according to tile number
                                    tileDraw.X = (int)tilesetNumber[tileMapLayer[i].data[j] - 1].X;
                                    tileDraw.Y = (int)tilesetNumber[tileMapLayer[i].data[j] - 1].Y;
                                    tileDraw.Width = (int)spriteTileSize.X;
                                    tileDraw.Height = (int)spriteTileSize.Y;

                                    //calculate layer depth in y axis by tile drawing (x+y = y depth)
                                    //default depth
                                    depth = 0.0f;
                                    //custom depth
                                    depth = tileMapLayer[i].depth / 100000f;
                                    //calculate tile position
                                    depth += (float)Math.Floor(drawposY / (girdSize.Y / 2)) / 1000f;
                                    depth += 0.01f;
                                    //calculate position on a tile (for tilemap, will use as layer order instead)
                                    depth += tileMapLayer[i].LayerOrder / 100000f;
                                    //if layer is vertical layer, set position on tile as half of map tile height
                                    depth += verticalDepth;

                                    //check if animate tile
                                    if (animateTiles != null && animateTiles.Count > 0)
                                    {
                                        //not good, new object every frame is not good
                                        //find the way to fix this
                                        AnimateTile animatetile = animateTiles.Where(item => item.tileID == (int)tileMapLayer[i].data[j] - 1).FirstOrDefault();
                                        if (animatetile != null)
                                        {
                                            //update animation
                                            animatetile.animation.Animate(animatetile.animationFrames, animatetile.speed);
                                            //draw
                                            spriteBatch.Draw(tileMapTexture, drawPos, animatetile.animation.GetSpriteAnimation(), Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, (float)depth);
                                        }
                                    }
                                    else
                                        spriteBatch.Draw(tileMapTexture, drawPos, tileDraw, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, (float)depth);
                                }

                                //clear 
                            }
                            else
                                if (j % mapSize.X == 0)
                                row++;
                    }
                }
            }
        }

        //add collision Box
        private void AddCollisionArea(Vector3 Position, Vector2 Size)
        {
            EntityTiled tiled = new EntityTiled();
            tiled.TextureName = "";
            tiled.Position = Position;
            tiled.activeScene = currentScene;
            if (currentScene.MapOrientation != MapOrientation.Isometric)
                tiled.collisionBox.Add(new CollisionBox(tiled, Size, Vector2.Zero, true, Position.Z));
            else
            {
                //set up entity position according to pseudo axis
                Vector2 newPos = OrthogonalToIsometricPosition(new Vector2(tiled.Position.X,tiled.Position.Y));
                tiled.Position = new Vector3(newPos.X, newPos.Y, Position.Z);
                //add sat collision box
                List<Vector2> edges = new List<Vector2>();
                edges.Add(Vector2.Zero);
                edges.Add(OrthogonalToIsometricPosition(new Vector2(0, Size.Y)));
                edges.Add(OrthogonalToIsometricPosition(new Vector2(Size.X, 0)));
                edges.Add(OrthogonalToIsometricPosition(new Vector2(Size.X, Size.Y)));
                tiled.SATcollisionBox.Add(new SATCollisionBox(tiled, edges, girdSize.Y, true));
            }
            currentScene.GetSceneEntityList().Add(tiled);
        }

        private void AddCustomEntity(string EntityName, Vector3 Position, Vector2 Size, string customProperties)
        {
            EntityTiled tiled = new EntityTiled();
            tiled.EntityName = EntityName;
            tiled.Position = Position;
            tiled.CustomProperties = customProperties;
            tiled.activeScene = currentScene;
            if(currentScene.MapOrientation != MapOrientation.Isometric)
                tiled.collisionBox.Add(new CollisionBox(tiled, Size, Vector2.Zero, true, girdSize.Y));
            else
            {
                //set up entity position according to pseudo axis
                Vector2 newPos = OrthogonalToIsometricPosition(new Vector2(tiled.Position.X, tiled.Position.Y));
                tiled.Position = new Vector3(newPos.X, newPos.Y, Position.Z);
                //add sat collision box
                List<Vector2> edges = new List<Vector2>();
                edges.Add(Vector2.Zero);
                edges.Add(OrthogonalToIsometricPosition(new Vector2(0, Size.Y)));
                edges.Add(OrthogonalToIsometricPosition(new Vector2(Size.X, 0)));
                edges.Add(OrthogonalToIsometricPosition(new Vector2(Size.X, Size.Y)));
                tiled.SATcollisionBox.Add(new SATCollisionBox(tiled, edges, girdSize.Y, true));
            }
            currentScene.GetSceneEntityList().Add(tiled);
        }

        private Vector2 OrthogonalToIsometricPosition(Vector2 orthogonalPos)
        {
            if (currentScene.MapOrientation == MapOrientation.Isometric)
                return new Vector2(orthogonalPos.X * ((girdSize.X / 2) / girdSize.Y), orthogonalPos.X * (girdSize.Y / girdSize.X)) + new Vector2(orthogonalPos.Y * ((girdSize.X / 2) / girdSize.Y) * -1, orthogonalPos.Y * (girdSize.Y / girdSize.X));

            return orthogonalPos;
        }
    }

    public class EntityTiled : CoreEntity
    {
        public EntityTiled()
            : base()
        {
            EntityName = "EntityTiled";
            StaticEntity = true;
            isDrawCollisionBox= true;
        }
    }

    public class TileMapLayer
    {
        public string name = null;
        public string type = null;
        public List<byte> data = null;
        public List<TileMapObject> objects = null;
        public float Z = 0;
        public float depth = 0.4f;
        public Vector2 tilePivot;
        public int LayerOrder = 0;
        public bool IsVerticalLayer;
    }
    public class TileMapObject
    {
        public string name = "";
        public float x = 0;
        public float y = 0;
        public float height = 0;
        public float width = 0;
        public string type = "";
    }
    class AnimateTile
    {
        public int tileID;
        public uint speed;
        public List<int> animationFrames;
        public Animation animation;

        public AnimateTile(Rectangle TextureSize, Vector2 SpriteCut)
        {
            animationFrames = new List<int>(0);
            animation = new Animation(TextureSize, SpriteCut);
        }
    }
    
    public enum MapOrientation
    {
        Unidentified,
        Orthogonal,
        Isometric
    }
}
