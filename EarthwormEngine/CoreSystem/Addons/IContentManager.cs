﻿/* IContentManager use for supporting custom content manager for specific platform
 * 
*/using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthwormEngine
{
    public interface IContentManager
    {
        string RootDirectory { get; set; }

        T Load<T>(string s);

        StreamReader StreamReader(string s);

        void Unload();
    }
}
