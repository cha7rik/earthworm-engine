﻿///*
///Earthworm Engine Content Manager
///Password encryption zipped content is supported (except music)
///Loading content from zip can be enable of disable in SceneManagerConfiguration, also encryption password
///The content file must be named "Content.dat" (rename extension from .zip to .dat)
///Since loading music from zip is not supported, prefer to leave the music files in Content folder (.xnb can be zipped)
///*

using System.IO;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using ICSharpCode.SharpZipLib.Zip;
using PCLStorage;
using Microsoft.Xna.Framework.Media;
using System.Reflection;

namespace EarthwormEngine
{
    public class ContentManager : Microsoft.Xna.Framework.Content.ContentManager, IContentManager
    {
        public ContentManager(GameServiceContainer gsc, string RootDirectory) : base(gsc)
        {
            this.RootDirectory = RootDirectory;
        }
        /*
        public override T Load<T>(string assetName)
        {
            if(typeof(T) == typeof(Song))
            {
                Console.WriteLine("it's a song");
            }

            return base.Load<T>(assetName);
        }
        */
        protected override Stream OpenStream(string assetName)
        {
            /*Disable due to cross platform issue
            * cannot check file location exists for now
            * 
            if (new File(Path.Combine(RootDirectory, assetName)).Exists())
            {
                return base.OpenStream(assetName); 
            }
            */
            return base.OpenStream(assetName);
        }

        /// <summary>
        /// Load any non-build content (custom content) from Monogame Content Pipeline
        /// </summary>
        /// <param name="path">File Path</param>
        /// <returns>StreamReader</returns>
        public virtual StreamReader StreamReader(string path)
        {
            return new StreamReader(TitleContainer.OpenStream(path));
        }
    }
}
