﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace EarthwormEngine.InputSystem
{
    /// <summary>
    /// Class handle all input form keyboard
    /// </summary>
    public static class KeyboardController
    {
        private static KeyboardState keyState;
        private static KeyboardState oldState;

        /// <summary>
        /// Update all keyboard input status
        /// Need to call after scene update
        /// </summary>
        public static void Update() //Need to call after scene update
        {
            oldState = Keyboard.GetState();
        }

        /// <summary>
        /// Check if key pressed
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is pressed</returns>
        public static bool KeyDown(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if key not pressed
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key not pressed</returns>
        public static bool KeyUp(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if Key was just hit
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key hit</returns>
        public static bool KeyHit(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(key) && oldState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if Key was just release
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key release</returns>
        public static bool KeyRelease(Keys key)
        {
            keyState = Keyboard.GetState();
            if (oldState.IsKeyDown(key) && keyState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Get multiple keys pressed
        /// </summary>
        /// <returns>List of pressed keys</returns>
        public static List<Keys> GetKeyPress()
        {
            List<Keys> res = null;
            keyState = Keyboard.GetState();
            res = new List<Keys>(keyState.GetPressedKeys());
            return res;
        }

        /// <summary>
        /// Get multiple keys hit
        /// </summary>
        /// <returns>List of hit keys</returns>
        public static List<Keys> GetKeyHit()
        {
            List<Keys> res;
            List<Keys> old;
            keyState = Keyboard.GetState();
            res = new List<Keys>(keyState.GetPressedKeys());
            old = new List<Keys>(oldState.GetPressedKeys());

            res.RemoveAll(item => old.Contains(item));
            return res;
        }
    }
}
