﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace EarthwormEngine.InputSystem
{
    public static class TouchController
    {
        private static TouchCollection touchState;
        private static TouchCollection oldState;
        private static List<TouchInfo> TouchInfoCollection;

        public static void Init()
        {
            TouchInfoCollection = new List<TouchInfo>();
        }

        /// <summary>
        /// Update new touch input status
        /// Need to call before scene update (SceneManager implemented)
        /// </summary>
        public static void RefreshTouchState()
        {
            //compare new TouchState to currentTouchState
            //check if touch is new or released, set flag on them
            touchState = TouchPanel.GetState();
            //for(int i = 0; i < )
            //update touch loc and check for new touch
            for (int i = 0; i < touchState.Count; i++)
            {
                //check if there are touchinfo in the list
                int collectionIndex = -1;
                collectionIndex = TouchInfoCollection.IndexOf(TouchInfoCollection.Find(x => x.ID == touchState[i].Id));
                //if no touch id exist, create new one
                if (collectionIndex == -1)
                {
                    TouchInfo newTouch = new TouchInfo();
                    newTouch.State = InputState.Hit;
                    newTouch.ID = touchState[i].Id;
                    //update touch loc
                    newTouch.touchLoc = touchState[i];
                    newTouch.PositionPressed = touchState[i].Position;

                    TouchInfoCollection.Add(newTouch);
                }
                else
                    //update touch loc
                    TouchInfoCollection[collectionIndex].touchLoc = touchState[i];

            }
            //check if released touch
            for (int i = 0; i < TouchInfoCollection.Count; i++)
            {
                TouchLocation t;
                //if there are new released in updated state, flag as released
                if (!touchState.FindById(TouchInfoCollection[i].ID, out t))
                {
                    TouchInfoCollection[i].State = InputState.Released;
                }
            }


        }

        /// <summary>
        /// Update old touch state
        /// Need to call after scene update (SceneManager implemented)
        /// </summary>
        public static void Update()
        {
            oldState = TouchPanel.GetState();
            //update collection flag
            for(int i = 0;i < TouchInfoCollection.Count; i++)
            {
                if (TouchInfoCollection[i].State == InputState.Hit)
                    TouchInfoCollection[i].State = InputState.Down;
                if (TouchInfoCollection[i].State == InputState.Released)
                    TouchInfoCollection.RemoveAt(i);
            }
        }

        /// <summary>
        /// Get current touch count
        /// </summary>
        /// <returns>Number of current touch count</returns>
        public static int GetTouchCount()
        {
            return TouchInfoCollection.Count;
        }

        /// <summary>
        /// Get touch move size form previous frame
        /// </summary>
        /// <param name="touchIndex">Touch index</param>
        /// <returns>Vector2 move size, return Vector.Zero if no touch activity</returns>
        public static Vector2 GetMoveSize(int touchIndex)
        {
            if (oldState.Count > touchIndex 
                && TouchInfoCollection.Count > touchIndex)
            {
                //check if new touch, return zero because no activity
                if (TouchInfoCollection[touchIndex].State != InputState.Hit)
                {
                    //find oldtouch by id, return zero if not found
                    TouchLocation oldTouch;
                    if (oldState.FindById(TouchInfoCollection[touchIndex].ID, out oldTouch))
                    {
                        return ((TouchInfoCollection[touchIndex].touchLoc.Position - oldTouch.Position) / new Vector2(ResolutionManager.getScale().X, ResolutionManager.getScale().Y));
                    }
                }
            }

            return Vector2.Zero;
        }

        /// <summary>
        /// Get touch position relate to screen
        /// </summary>
        /// <param name="touchIndex">Touch index</param>
        /// <returns>>Vector2 position, return Vector.Zero if no touch activity</returns>
        public static Vector2 GetTouchPosition(int touchIndex)
        {
            if (TouchInfoCollection.Count > touchIndex)
            {
                return TouchInfoCollection[touchIndex].touchLoc.Position - ResolutionManager.getMarginSize();
            }

            return Vector2.Zero;
        }

        /// <summary>
        /// Get touch position relate to game world
        /// </summary>
        /// <param name="touchIndex">Touch index</param>
        /// <param name="sceneCamera">Scene camera object</param>
        /// <returns>>Vector2 world position, return Vector.Zero if no touch activity</returns>
        public static Vector2 GetTouchPosition(int touchIndex, Camera sceneCamera)
        {
            if (TouchInfoCollection.Count > touchIndex)
            {
                //get TouchLocation info
                TouchLocation touch = TouchInfoCollection[touchIndex].touchLoc;
                return Vector2.Transform(touch.Position, Matrix.Invert(sceneCamera.GetCameraTranformation())) - (ResolutionManager.getMarginSize() / sceneCamera.GetScale());
            }

            return Vector2.Zero;
        }

        public static InputState GetTouchState(int touchIndex)
        {
            if (TouchInfoCollection.Count > touchIndex)
                return TouchInfoCollection[touchIndex].State;
            else
                return InputState.Invalid;
        }

        private static int GetTouchID(int touchIndex)
        {
            if (TouchInfoCollection.Count > touchIndex)
                return TouchInfoCollection[touchIndex].ID;
            else
                return -1;

        }
    }

    class TouchInfo
    {
        public InputState State { get; set; }
        public int ID { get; set; }
        public TouchLocation touchLoc { get; set; }
        public Vector2 PositionPressed { get; set; }
    }

    /// <summary>
    /// Input state for mouse and touch data
    /// </summary>
    public enum InputState
    {
        Invalid,
        Hit,
        Down,
        Released,
        Up
    }
}
