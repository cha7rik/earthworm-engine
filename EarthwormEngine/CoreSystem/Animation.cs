﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormEngine
{ 
    /// <summary>
    /// This class handle all game animation such as frame counter, sprite cut, etc. and create draw position on sprite texture
    /// </summary>
    public class Animation
    {
        public Rectangle textureSize { get; private set; }
        public Vector2 spriteCut { get; private set;}
        public int CurrentFrame { get; private set; }
        private uint frameCounter;
        private List<Vector2> frames;
        private Rectangle SpriteAnimation;
        private uint frameListCounter;

        /// <summary>
        /// Create Animation class
        /// </summary>
        /// <param name="TextureSize">Usage area of texture</param>
        /// <param name="SpriteCut">Size of a single frame (Will apply to whole spritesheet)</param>
        public Animation(Rectangle TextureSize,Vector2 SpriteCut)
        {
            textureSize = TextureSize;
            spriteCut = SpriteCut;
            //to prevent blank image, set spriteCut as texture size if spriteCut is not initiate
            if (spriteCut == Vector2.Zero)
                spriteCut = new Vector2(textureSize.Width, textureSize.Height);
            frameCounter = 100;
            frameListCounter = 0;
            //split to frame
            //calculate total x,y
            int totalX = (int)(textureSize.Width / spriteCut.X);
            int totalY = (int)(textureSize.Height / spriteCut.Y);
            SpriteAnimation = new Rectangle();

            frames = new List<Vector2>();

            for (int i = 0; i < totalY; i++)
                for (int j = 0; j < totalX; j++)
                    frames.Add(new Vector2(spriteCut.X * j,spriteCut.Y * i));
        }
        /// <summary>
        /// Set animation frame by number.
        /// </summary>
        /// <param name="FrameNumber">Number of frame.</param>
        /// <returns>Set result</returns>
        public bool SetFrame(int FrameNumber)
        {
            if (FrameNumber < frames.Count)
            {
                CurrentFrame = FrameNumber;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Set animation frame by column and row of sprite sheet.
        /// </summary>
        /// <param name="column">Column index</param>
        /// <param name="row">Row index</param>
        /// <returns>Set result</returns>
        public bool SetFrame(int column, int row)
        {
            if (column < frames.Count / (textureSize.X / spriteCut.X) && row < frames.Count / (textureSize.Y / spriteCut.Y))
            {
                int frameIndex = (int)(row / (textureSize.Y / spriteCut.Y));
                frameIndex += (int)(column * (row / (textureSize.X / spriteCut.X)));
                CurrentFrame = frameIndex;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Animate sprite form start frame number to end frame number
        /// This should be called in update method
        /// </summary>
        /// <param name="FrameStart">Number of first frame.</param>
        /// <param name="FrameEnd">Number of last frame, must be more value than FrameStart</param>
        /// <param name="AnimateSpeed">Speed of animation(count by frame time)</param>
        public void Animate(int FrameStart, int FrameEnd, uint AnimateSpeed)
        {
            if (FrameStart < FrameEnd)
            {
                if (CurrentFrame < FrameStart || CurrentFrame > FrameEnd)
                    CurrentFrame = FrameStart;
                if (frameCounter < AnimateSpeed)
                    frameCounter++;
                else
                {
                    CurrentFrame++;
                    frameCounter = 0;
                    if (CurrentFrame > FrameEnd)
                        CurrentFrame = FrameStart;
                }
            }
            else if (FrameStart > FrameEnd)
            {
                if (CurrentFrame > FrameStart || CurrentFrame < FrameEnd)
                    CurrentFrame = FrameStart;
                if (frameCounter < AnimateSpeed)
                    frameCounter++;
                else
                {
                    CurrentFrame--;
                    frameCounter = 0;
                    if (CurrentFrame < FrameEnd)
                        CurrentFrame = FrameStart;
                }
            }
            else
                CurrentFrame = FrameStart;
        }

        /// <summary>
        /// Animate sprite form list for frames.
        /// This should be called in update method.
        /// </summary>
        /// <param name="FrameList">List contain frame numbers</param>
        /// <param name="AnimateSpeed">Speed of animation.(count by frame time)</param>
        public void Animate(List<int> FrameList, uint AnimateSpeed)
        {
            if (frameListCounter < 0 || frameListCounter > FrameList.Count)
                frameListCounter = 0;
            if (frameCounter < AnimateSpeed)
                frameCounter++;
            else
            {
                frameListCounter++;
                frameCounter = 0;
                if (frameListCounter > FrameList.Count - 1)
                    frameListCounter = 0;
            }

            CurrentFrame = FrameList[(int)frameListCounter];
        }

        /// <summary>
        /// Get rectangle position on spritesheet for draw method. Used by SpriteBatch.Draw(...)
        /// </summary>
        /// <returns>Rectangle position</returns>
        public Rectangle GetSpriteAnimation()
        {
            if (frames.Count <= 1)
                return new Rectangle(textureSize.X,textureSize.Y, (int)spriteCut.X, (int)spriteCut.Y);

            SpriteAnimation.X = textureSize.X + (int)frames[CurrentFrame].X;
            SpriteAnimation.Y = textureSize.Y + (int)frames[CurrentFrame].Y;
            SpriteAnimation.Width = (int)spriteCut.X;
            SpriteAnimation.Height = (int)spriteCut.Y;
            return SpriteAnimation;
        }

        /// <summary>
        /// Get total frame of the sprite texture calculate by frame size
        /// </summary>
        /// <returns>Total frame</returns>
        public int GetTotalFrame()
        {
            return frames.Count;
        }

        /// <summary>
        /// Set new sprite cut
        /// </summary>
        /// <param name="TextureSize">Usage area of texture</param>
        /// <param name="SpriteCut">Vector2 size of a single frame (Will apply to whole spritesheet)</param>
        public void RecalculateSpriteAnimation(Rectangle TextureSize,Vector2 SpriteCut)
        {
            spriteCut = SpriteCut;
            textureSize = TextureSize;
            //split to frame
            //calculate total x,y
            int totalX = (int)(textureSize.Width / spriteCut.X);
            int totalY = (int)(textureSize.Height / spriteCut.Y);
            SpriteAnimation = new Rectangle();

            frames = new List<Vector2>();

            for (int i = 0; i < totalY; i++)
                for (int j = 0; j < totalX; j++)
                    frames.Add(new Vector2(spriteCut.X * j, spriteCut.Y * i));
        }
    }
}
