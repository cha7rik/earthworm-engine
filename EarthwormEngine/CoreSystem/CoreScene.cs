﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using EarthwormEngine.ParticleSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// Scene class, manage all entity, particle, etc. for one scene
    /// </summary>
    public class CoreScene
    {
        public SceneManager sceneManager { get; private set; }
        /// <summary>
        /// All enetity in scene must be registed here
        /// </summary>
        protected EntityManager entityManager;
        /// <summary>
        /// All userinterface in scene must be registed here
        /// </summary>
        protected List<UserInterface> UserInterfaceList;
        /// <summary>
        /// All particle effect in scene must be registed here
        /// </summary>
        protected EffectManager effectManager;
        protected IContentManager contentManager;
        /// <summary>
        /// Scene tile map
        /// </summary>
        public TileMap tiledMap { get; protected set; }
        protected Texture2D overlayTexture;
        /// <summary>
        /// Tile map orientation
        /// </summary>
        public MapOrientation MapOrientation { get; protected set; }

        /// <summary>
        /// Set overlay color (currently used as ambient light)
        /// </summary>
        public Color OverlayColor { get; set; }
        /// <summary>
        /// Set overlay opacity (currently used as ambient light)
        /// </summary>
        public float OverlayOpacity { get; set; }
        /// <summary>
        /// Set overlay depth (currently used as ambient light)
        /// </summary>
        public float OverlayColorDepth { get; set; }

        public GraphicsDevice GraphicDevice { get { return sceneManager.GraphicsDevice; } }

        public Camera MainCamera;

        /// <summary>
        /// Create new scene
        /// </summary>
        public CoreScene(MapOrientation orientation = MapOrientation.Orthogonal)
        {
            OverlayColorDepth = 0.8f;
            OverlayOpacity = 0.0f;
            MapOrientation = orientation;
        }

        /// <summary>
        /// Create new scene
        /// </summary>
        /// <param name="SceneManager">SceneManager</param>
        public CoreScene(SceneManager SceneManager, MapOrientation orientation = MapOrientation.Orthogonal)
        {
            sceneManager = SceneManager;
            OverlayColorDepth = 0.8f;
            OverlayOpacity = 0.0f;
            MapOrientation = orientation;
        }

        /// <summary>
        /// Initialize scene
        /// </summary>
        public virtual void Init()
        {
            contentManager = sceneManager.GetContentManager();
            entityManager = new EntityManager(sceneManager.GetContentManager());
            UserInterfaceList = new List<UserInterface>();
            effectManager = new EffectManager(sceneManager);
            OverlayColor = new Color(new Vector3(1, 1, 1));
            overlayTexture = new Texture2D(sceneManager.GraphicsDevice, 1, 1);

            MainCamera = new Camera();
            effectManager.Init();
            foreach (UserInterface ui in UserInterfaceList)
                ui.Init();
        }
        /// <summary>
        /// Load scene and component content
        /// </summary>
        public virtual void Load()
        {
            entityManager.Load();
            overlayTexture.SetData(new Color[] { Color.White });
            foreach (UserInterface ui in UserInterfaceList)
                ui.Load(contentManager);
        }
        /// <summary>
        /// Update scene and component
        /// </summary>
        public virtual void Update()
        {
            MainCamera.Update();
            foreach (UserInterface ui in UserInterfaceList)
                ui.Update();
            entityManager.Update();
            effectManager.Update();
        }

        /// <summary>
        /// Draw userinterfaces that exclude zooming
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        public virtual void DrawUserInterface(SpriteBatch spriteBatch)
        {
            foreach (UserInterface ui in UserInterfaceList)
                ui.Draw(spriteBatch);
        }

        /// <summary>
        /// Draw overlay color
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        public virtual void DrawOverlayColor(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(overlayTexture, Vector2.Zero, new Rectangle(0, 0, (int)MainCamera.GetPosition().X + (int)Calculator.GetScreenSize().X, (int)MainCamera.GetPosition().Y + (int)Calculator.GetScreenSize().Y), OverlayColor * OverlayOpacity, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, OverlayColorDepth);
        }

        /// <summary>
        /// Draw scene and component (No need to call SpriteBatch.Begin)
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            Rectangle cameraViewPort = MainCamera.GetCameraViewPort();

            float tileSizeY = 0;
            if (tiledMap != null)
            {
                tiledMap.Draw(spriteBatch, cameraViewPort);
                tileSizeY = tiledMap.girdSize.Y;
            }

            entityManager.Draw(spriteBatch , cameraViewPort, MapOrientation, tileSizeY);
        }

        /// <summary>
        /// Separate draw particle from Scene.Draw()
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        /// <param name="isUIParticle">Is draw particle UI or not (for draw on ui layer)</param>
        public void DrawParticle(SpriteBatch spriteBatch, bool isUIParticle)
        {
            effectManager.Draw(spriteBatch, MainCamera.GetCameraViewPort(), isUIParticle);
        }
        /// <summary>
        /// Unload scene component
        /// </summary>
        public virtual void Unload()
        {
            //this.Dispose(true);
            sceneManager.GetContentManager().Unload();
        }

        /// <summary>
        /// Get scene entity array
        /// </summary>
        /// <returns>List of entity</returns>
        public virtual List<CoreEntity> GetSceneEntityList()
        {
            return entityManager.entityList;
        }

        /// <summary>
        /// Get scene entity array with filter
        /// </summary>
        /// <param name="filter">Predicate</param>
        /// <returns>List of entity</returns>
        public virtual List<CoreEntity> GetSceneEntityList(Predicate<CoreEntity> filter)
        {
            return entityManager.entityList.FindAll(filter);
        }

        /// <summary>
        /// Add new entity to scene
        /// </summary>
        /// <param name="ent">Entity</param>
        /// <param name="pos">Entity position</param>
        /// <param name="angle">Sprite rotation angle</param>
        /// <param name="scene">Scene that contain this entity</param>
        public T AddEntity<T>(T ent, Vector3 pos, float angle) where T : CoreEntity
        {
            entityManager.AddEntity(ent, pos, angle, this);
            return ent;
        }

        /// <summary>
        /// Add new entity to scene
        /// </summary>
        /// <param name="entName">Entity name</param>
        /// <param name="ent">Entity</param>
        /// <param name="pos">Entity position</param>
        /// <param name="angle">Sprite rotation angle</param>
        /// <param name="scene">Scene that contain this entity</param>
        public T AddEntity<T>(string entName, T ent, Vector3 pos, float angle) where T : CoreEntity
        {
            entityManager.AddEntity(entName, ent, pos, angle, this);
            return ent;
        }

        /// <summary>
        /// Seek entity in array by its name
        /// </summary>
        /// <param name="entityName">Entity name</param>
        /// <returns></returns>
        public CoreEntity SeekEntity(string entityName)
        {
            return entityManager.SeekEntity(entityName);
        }
        /// <summary>
        /// Seek entity in array by its ID
        /// </summary>
        /// <param name="EntityID">Entity ID</param>
        /// <returns></returns>
        public CoreEntity SeekEntity(uint EntityID)
        {
            return entityManager.SeekEntity(EntityID);
        }

        /// <summary>
        /// Register a particle effect to scene
        /// </summary>
        /// <param name="particleEffect">Particle effect to add</param>
        /// <param name="isUIParticle">Is create particle on UI layer</param>
        public void AddParticleEffect(ParticleEffect particleEffect, bool isUIParticle)
        {
            effectManager.Add(particleEffect,isUIParticle);
        }

        /// <summary>
        /// Register a particle effect to scene
        /// </summary>
        /// <param name="path">Particle effect file path</param>
        /// <param name="position">Emit postion</param>
        public void AddParticleEffect(string path, Vector3 position)
        {
            effectManager.Add(path, position, false);
        }

        /// <summary>
        /// Register a particle effect to scene
        /// </summary>
        /// <param name="path">Particle effect file path</param>
        /// <param name="position">Emit postion</param>
        /// <param name="depth">Layer depth</param>
        public void AddParticleEffect(string path, Vector3 position, float depth)
        {
            ParticleEffect p = new ParticleEffect(path, effectManager, contentManager);
            p.EmitPosition = position;
            p.Depth = depth;
            effectManager.Add(p,false);
        }

        /// <summary>
        /// Get scene effect manager
        /// </summary>
        /// <returns>Effect manager</returns>
        public EffectManager GetEffectManager()
        {
            return effectManager;
        }

        /// <summary>
        /// Get scene content manager
        /// </summary>
        /// <returns>Content manager</returns>
        public IContentManager GetContentManager()
        {
            return sceneManager.GetContentManager();
        }
		
		/// <summary>
        /// Load new scene
        /// </summary>
        /// <param name="scene">Scene to load</param>
        public void LoadScene(CoreScene scene)
        {
            sceneManager.LoadScene(scene);
        }

        /// <summary>
        /// Set Active SceneManager, only used by SceneManager
        /// </summary>
        /// <param name="sm">Active Scene Manager</param>
        public void SetActiveSceneManager(SceneManager sm)
        {
            sceneManager = sm;
        }

        /// <summary>
        /// Convert orthogonal position to isometric position (this method only works on isometric map)
        /// </summary>
        /// <param name="orthogonalPos"></param>
        /// <returns>Vector 2 isometric position</returns>
        public Vector2 OrthogonalToIsometricPosition(Vector2 orthogonalPos)
        {
            if (MapOrientation == MapOrientation.Isometric)
                return new Vector2(orthogonalPos.X * ((tiledMap.girdSize.X / 2) / tiledMap.girdSize.Y), orthogonalPos.X * (tiledMap.girdSize.Y / tiledMap.girdSize.X)) + new Vector2(orthogonalPos.Y * ((tiledMap.girdSize.X / 2) / tiledMap.girdSize.Y) * -1, orthogonalPos.Y * (tiledMap.girdSize.Y / tiledMap.girdSize.X));

            return orthogonalPos;
        }
        /// <summary>
        /// Convert given grid position to raw position with option for snap to center of the grid
        /// </summary>
        /// <param name="gridPositon">Position according to grid map</param>
        /// <param name="isCentered">Return the raw position at center of the grid</param>
        /// <returns>Raw positon</returns>
        public Vector2 GetRawPositionByGridPosition(Vector2 gridPositon,bool isCentered)
        {
            Vector2 halfgrid = Vector2.Zero;
            if (isCentered)
                halfgrid = new Vector2(0,tiledMap.girdSize.Y/2);
            if (MapOrientation == MapOrientation.Isometric)
                return OrthogonalToIsometricPosition(gridPositon * new Vector2(tiledMap.girdSize.X / (tiledMap.girdSize.X / tiledMap.girdSize.Y), tiledMap.girdSize.Y)) + halfgrid;
            else if (MapOrientation == MapOrientation.Orthogonal)
                return new Vector2(gridPositon.X * tiledMap.girdSize.X, gridPositon.Y * tiledMap.girdSize.Y) + halfgrid;
            else
            {
                Debug.WriteLine("[WARNING] Scene.GetRawPositionByGridPosition : tileMap.mapOrientation is undentified.");
                return Vector2.Zero;
            }
        }
    }
}
