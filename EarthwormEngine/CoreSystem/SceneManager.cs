﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using EarthwormEngine.InputSystem;

namespace EarthwormEngine
{
    /// <summary>
    /// Scene manager is a main part of game. this class is for manage current scene.
    /// </summary>
    public class SceneManager : Game
    {
        protected GraphicsDeviceManager graphics;
        protected RenderTarget2D renderTarget;
        protected SpriteBatch spriteBatch;
        protected CoreScene currentScene;
        protected SceneManagerConfiguration config;
        public new IContentManager Content;

        /// <summary>
        /// Create new Scene Manager
        /// </summary>
        public SceneManager(SceneManagerConfiguration sceneManagerConfig)
        {
            config = sceneManagerConfig;
            PlatformManager.SetActivePlatform(config.ActivePlatform);

            //is fix FPS
            IsFixedTimeStep = config.LockFPSLimit;

            graphics = new GraphicsDeviceManager(this);
            ResolutionManager.Init(ref graphics);

            // Change Virtual Resolution 
            //actual game resolution
            ResolutionManager.SetVirtualResolution((int)config.GameResolution.X, (int)config.GameResolution.Y);

            //windows resolution
            //if on android platform, prefer to set the windows screen as device resolution and fullscreen
            if (PlatformManager.ActivePlatform == PlatformDevice.ANDROID)
            {
                graphics.IsFullScreen = true;
                graphics.SupportedOrientations = config.ScreenOrientation;

                if (config.ScreenOrientation == DisplayOrientation.Portrait)
                    //swap width and height on portrait orientation
                    ResolutionManager.SetResolution(graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth, true);
                else if (config.ScreenOrientation == DisplayOrientation.LandscapeLeft || config.ScreenOrientation == DisplayOrientation.LandscapeRight)
                    ResolutionManager.SetResolution(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, true);
                //display warning if screen orientation is not set
                else if (config.ScreenOrientation == DisplayOrientation.Unknown)
                    Debug.WriteLine("[WARNING] SceneManager : Screen orientation is not set, this may causes visual glitch on some devices.");
                    
            }
            else
            {
                graphics.IsFullScreen = config.IsFullScreen;
                ResolutionManager.SetResolution((int)config.WindowResolution.X, (int)config.WindowResolution.Y, config.IsFullScreen);
            }
            
            this.IsMouseVisible = config.IsMouseVisible;
            Mouse.WindowHandle = Window.Handle;
            Window.Title = config.WindowTitle;
            //Enable of disable zipped content
            /* Zip opertaion is disabled due to cross platform issue.
             * 
            if (!string.IsNullOrEmpty(config.SetContentZipPassword))
                Content = new ContentManager(Services, "Content", config.SetContentZipPassword);
            else
                Content = new ContentManager(Services, "Content");
            */
            //register custom content manager
            Content = new ContentManager(Services, "Content");
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //initialization logic
            Calculator.Init(this);

            //#if WINDOWS || OPENGL
            //            KeyboardInput.Update();
            //            MouseInput.Update();
            //#elif ANDROID
            TouchController.Init();
            TouchController.Update();
            //#endif

            /*
             * Due to DirectX issue, controller by SlimDX is disable for now
             * //ControllerInput.Init();
            */
            
            //set start scene
            currentScene = config.InitialScene;
            currentScene.SetActiveSceneManager(this);
            currentScene.Init();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content. This will load all scene contents include entities contents within scene.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            currentScene.Load();
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            //    this.Exit();

            Calculator.Update(gameTime);

            TouchController.RefreshTouchState();

            currentScene.Update();

            base.Update(gameTime);

            if (PlatformManager.ActivePlatform == PlatformDevice.DESKTOPGL
                || PlatformManager.ActivePlatform == PlatformDevice.WINDOWS
                || PlatformManager.ActivePlatform == PlatformDevice.MAC)
            {
                KeyboardController.Update();
                MouseController.Update();
            }
            else if (PlatformManager.ActivePlatform == PlatformDevice.ANDROID
                || PlatformManager.ActivePlatform == PlatformDevice.IOS)
            {
                TouchController.Update();
            }
            else
                Debug.WriteLine("SceneManager : Platform is unsupport for input update");
            //#endif

            /*
             * Due to DirectX issue, controller by SlimDX is disable for now
            ControllerInput.Update();
            */
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            config.Draw(spriteBatch, currentScene);

            base.Draw(gameTime);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            //Unload any non ContentManager content
            /*
             * Due to DirectX issue, controller by SlimDX is disable for now
            ControllerInput.Unload();
            */
        }

        public virtual void LoadScene(CoreScene scene)
        {
            //unload current scene
            currentScene.Unload();

            //init new scene
            currentScene = scene;
            currentScene.SetActiveSceneManager(this);
            currentScene.Init();
            LoadContent();
        }

        /// <summary>
        /// Get current active scene
        /// </summary>
        /// <returns>Active scene</returns>
        public CoreScene GetActiveScene()
        {
            return currentScene;
        }

        public virtual IContentManager GetContentManager()
        {
            return Content;
        }
    }
}
