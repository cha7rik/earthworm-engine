using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using EarthwormEngine;
using EarthwormEngine.Scenes;

namespace Android
{
    [Activity(Label = "Android"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/Theme.Splash"
        , AlwaysRetainTaskState = true
        , LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
        , ScreenOrientation = ScreenOrientation.Portrait //Must not forget to set this
        , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize | ConfigChanges.ScreenLayout)]
    public class Activity1 : Microsoft.Xna.Framework.AndroidGameActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Hide navigation bar
            SystemUiFlags flags = SystemUiFlags.HideNavigation | SystemUiFlags.Fullscreen | SystemUiFlags.ImmersiveSticky | SystemUiFlags.Immersive;
            Window.DecorView.SystemUiVisibility = (StatusBarVisibility)flags;
            Immersive = true;

            SceneManagerConfiguration config = new SceneManagerConfiguration();
            config.BackgroundColor = Microsoft.Xna.Framework.Color.CornflowerBlue;
            config.InitialScene = new SceneTest();
            config.IsFullScreen = true;
            //WindowsResolution must be a device's actual resolution (SceneManager will implement this anyway)
            config.WindowResolution = new Microsoft.Xna.Framework.Vector2(2280, 1080);
            config.GameResolution = new Microsoft.Xna.Framework.Vector2(540, 1140);
            //set
            config.ActivePlatform = PlatformDevice.ANDROID;
            //set screen orientation (this for resolution manager to handle)
            config.ScreenOrientation = Microsoft.Xna.Framework.DisplayOrientation.Portrait;

            var g = new SceneManager(config);
            View _view = g.Services.GetService(typeof(View)) as View;
            _view.SystemUiVisibility = (StatusBarVisibility)flags;

            SetContentView(_view);
            g.Run();
        }
    }
}

